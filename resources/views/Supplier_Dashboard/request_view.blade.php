@extends('theme.default')


@section('content')
<!-- <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Invoice View</h1>
    </div>
</div> -->
<style type="text/css">
    .modal-backdrop{
        z-index: -1;
    }
</style>
<?php 


$id = \Request::segment(2);
 if(Auth::user()->role == 'supplier'){
    session()->put('id', $id);
 }

$request_data = DB::table('request')
                ->where('id','=',$id)
                ->first();
               
if($bid_data = DB::table('placedbid')
                ->where(['req_id'=>$id,'user_id'=>Auth::user()->id])
                ->get()){
}

 $get_status = DB::table('status')->where('request_id','=',$id)->first(); 
  $showPostNow = '';
  $getAbilityToPost = DB::table('invoicesign')->where(['reqId'=> $id, 'userId'=>Auth::user()->id])->get();  
  $reqCount  = count($getAbilityToPost); 
  $totalSign = 0;
  foreach($getAbilityToPost as $findSIgns) {
  	if($findSIgns->signdone == 1){
  		$totalSign++;
  	}
  }  
  if($totalSign == $reqCount){
  	$showPostNow = 1;
  }  else{
  	$showPostNow = 0;
  }    
 
$getSigners = DB::table('users')->where('id','!=',Auth::user()->id)->where('role','!=', 'admin')->where('role','!=', 'bank')->get();
$checkAllSigned = DB::table('invoicesign')->where('reqId','=',$id)->get(); 
$checkAllSignDone = DB::table('invoicesign')->where(['reqId'=>$id,'signdone' => 1])->get();                                         
//print_r($getAbilityToPost);
?>

 @if (\Session::has('success'))
    <div class="alert alert-info">
        <ul style="margin: 0;list-style: none;">
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div id="request_view" class="container request_view well ">

        <div class="row">
            <div class="col-md-12"><h3 class="center highlight"><b>Bill of Exchange</b></h3></div>
            <div class="col-md-4">
                <div class="form-group">
	                <label class="normal_color">{{$request_data->supplier_place}},</label>
                    <label class="normal_color"> {{date('d-m-Y', strtotime($request_data->created_at))}}</label>
            	</div>
            </div>
         <div class="col-md-4">
                <div class="form-group">
	                <label class="highlight">Amount</label>
                    <label class="normal_color">IN Rs.{{$request_data->amount_numbers}}/-</label>
            	</div>
    	</div>
    	 <div class="col-md-4">
            <div class="form-group">
	                <label class="highlight">At</label>
                    <label class="normal_color">{{$request_data->no_of_days}} days after sight</label>
            	</div>
    	</div>
        <div class="col-md-8">
                <div class="form-group">
	                <label class="highlight">Pay against this Sole Bill of Exchange to the order of</label>
            	</div>
    	</div>
    	
        <div class="col-md-4">
                <div class="form-group">
	                <label class="normal_color">Ourselves</label>
            	</div>
    	</div>
    	 <div class="col-md-6">
    	</div>
        <div class="col-md-12"></div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">the sum of</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label class="normal_color">In Rs. {{$request_data->amount_words}}</label>
            	</div>
    	</div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">for value</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label class="normal_color">Received</label>
            	</div>
    	</div>
        <div class="col-md-6">
                <div class="form-group">
	                <label class="highlight">To:</label>
                    <p>{{$request_data->buyer_address}}</p>
                    <p>{{$request_data->buyer_name}}</p>
                     <p>{{$request_data->buyers_email}}</p>
            	</div>
    	</div>
    	 <div class="col-md-6 center">
                <div class="form-group">
	                <label class="highlight">For and on behalf of:</label>
                    <p>{{$request_data->payee}}</p>
                    <p>{{$request_data->supplier_place}}</p>
            	</div>
    	</div>

        </div>

        <div class="row">
   
        </div>
</div>

<div class="container request_view">
    <div class="row">
    <div class="col-md-6 left">
        <div class="form-group">
            
           <!--  <a href="#" ><form action="/bankDecline" method="post" enctype="multipart/form-data" name="decline">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
             <input class="form-control bidBudget" name="supplier_id" id="supplier_id" type="hidden" value="{{$request_data->user_id}}" >
             <input class="form-control bidBudget" name="supplier_amount" id="supplier_amount" type="hidden" value="{{$request_data->amount_numbers}}" >
             <input class="form-control bidBudget" name="req_id" id="req_id" type="hidden" value="{{$request_data->id}}" >
                <button type="submit" class="btn btn-danger">Decline</button>
            </form></a> -->
            
        </div>
    </div>
    <div class="col-md-6 right">
        <div class="form-group">
            @if(Auth::user()->role == 'supplier')
                @if($request_data->is_signed == 0)
                <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal6">Sign Invoice</a>
                @elseif($request_data->is_invite_sent == 0)
                <!-- <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal7">Invite Signers</a> -->
                <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal61">Buyer's Sign Awaited</a>
                @elseif($request_data->is_posted == 0)
                 <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal5">Post</a>
                 @else
                 <a href="#" disabled class="btn btn-primary">Already Posted</a>
                @endif
            @elseif(Auth::user()->role == 'bank')
                 @if(count($bid_data) > 0)
                    @if($get_status->status == 'bank_declined')
                    <a href="#" class="btn btn-primary" disabled type="button"  data-toggle="modal" data-target="#">Rejected</a>
                    @elseif($get_status->status == 'accepted')<a href="#" class="btn btn-primary" disabled type="button"  data-toggle="modal" data-target="#">Sold Out</a>
                    @else
                    <a href="#" class="btn btn-primary" type="button" disabled data-toggle="modal" data-target="">Already Quoted</a>
                    @endif
                 @else
                    @if($get_status->status == 'bank_declined')
                    <a href="#" class="btn btn-primary" disabled type="button"  data-toggle="modal" data-target="#">Rejected</a>
                    @elseif($get_status->status == 'accepted')<a href="#" class="btn btn-primary" disabled type="button"  data-toggle="modal" data-target="#">Sold Out</a>
                    @else
                    <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal4">Place Quote</a>
                    @endif
                 @endif
            @endif
        </div>
    </div>
    </div>
</div>
@if(!empty($attchments))
    <div class="row">
        <div class="col-md-12">
            <p>Attachments :</p>
        </div>
        
        @foreach($attchments as $atch)
        <div class="col-md-4" style="border: 1px solid lightgray;">
            @php $ext = pathinfo($atch, PATHINFO_EXTENSION); @endphp
            @if($ext == 'pdf')
            <object data='{{$atch}}' 
                type='application/pdf' 
                width='100%' 
                height='100%'>
                <p style="    height: 100%;margin-bottom: 0">PDF view is not supported in this browsrer, please open in another one.</p>
            </object>
            @elseif($ext == 'png')
            <img  width="100%" height="100%" src="{{$atch}}">
            @else
            <img  width="100%"  height="100%" src="{{$atch}}">
            @endif
            <button style="position: absolute;    bottom: 0;left: 0;"  data-url="{{$atch}}" class="btn btn-info view" >View</button>
        </div>
        @endforeach  
    </div>
     @endif
<div id="myModal5" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Post Invoice</h4>
            </div>
            <div class="modal-body row">
               <?php if($checkAllSigned == $checkAllSignDone){ ?>
                 <div class="modal-footer" style="text-align: center;">
                     <form  method="post" action="/bidType" enctype="multipart/form-data" name="placeBid">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                         <input type="hidden" id="" class="" name="reqId" value="{{$id}}">
                        <input type="hidden" id="" class="" name="bidType" value="Open Bid">
                          <input type="submit" id="placeBid" class="btn btn-success" value="Open Bid">
                    </form>
                    <form  method="post" action="/bidType" enctype="multipart/form-data" name="placeBid">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                         <input type="hidden" id="" class="" name="reqId" value={{$id}}>
                        <input type="hidden" id="" class="" name="bidType" value="Close Bid">
                          <input type="submit" id="placeBid" class="btn btn-success" value="Close Bid">
                    </form>
                </div>
               <?php  }else{ ?>
               <p style="text-align: center;"> Sorry unable to post, all signs are not completed.</p>
               <?php } ?>
               
             
        </div>
        </div>
    </div>
</div>

<div id="myModal6" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Make Sign</h4>
            </div>
            <div class="modal-body row">
               Waiting for Adhaar E-sign.
                <div class="modal-footer" style="text-align: center;">
                <!--  <form  method="post" action="/bidType" enctype="multipart/form-data" name="placeBid">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                     <input type="hidden" id="" class="" name="reqId" value="{{$id}}">
                    <input type="hidden" id="" class="" name="bidType" value="Open Bid">
                      <input type="submit" id="placeBid" class="btn btn-success" value="Open Bid">
                </form>
                <form  method="post" action="/bidType" enctype="multipart/form-data" name="placeBid">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                     <input type="hidden" id="" class="" name="reqId" value={{$id}}>
                    <input type="hidden" id="" class="" name="bidType" value="Close Bid">
                      <input type="submit" id="placeBid" class="btn btn-success" value="Close Bid">
                </form> -->
                </div>
             
        </div>
        </div>
    </div>
</div>

<div id="myModal61" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                 <h4 class="modal-title bidtitle">Buyer's Sign Awaited</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-6">
                   <p> Waiting for Buyer's Signature.</p>
                </div>
                <div class="col-md-6" style="text-align: right;">
                    <form  method="post" action="/reminderMail" name="reminderMail">
                         <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                         <input type="hidden" id="" class="" name="reqId" value={{$id}}>
                         <input type="submit" id="reminderMail" class="btn btn-info" value="Send Reminder">
                    </form> 
                </div>
              
               
             
        </div>
        </div>
    </div>
</div>

<div id="myModal7" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Available Signers</h4>
            </div>
         <form  method="post" action="/send-invites" enctype="multipart/form-data" name="placeBid">
            <div class="modal-body row">
                <div class="tab-pane col-md-12">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                     <input type="hidden" id="" class="" name="reqId" value="{{$id}}">
                        <table class="table">
                            <tbody>
                        <?php foreach ($getSigners as $k => $v) { ?>
                           <tr>
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="signers[]" value="{{$v->id}}" id="signer{{$v->id}}">
                                            <span class="form-check-sign">
                                                <span class="check"></span>
                                            </span>
                                            {{$v->name}} ({{$v->email}})
                                        </label>
                                        <label for="signer{{$v->id}}"></label>
                                    </div>
                                </td>
                            </tr>
                        <?php  } ?>
                            </tbody>
                        </table>
                        <input type="submit" id="placeBid" class="btn btn-success" value="Send Invites">
                    </div>
                
            </div>
        </div>
        </div>
    </div>
</div>


<div id="myModal4" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
  
    <form style="visibility: hidden;">
</form>
    <form  method="post" action="/placeBid" name="placeBid">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Place Bid</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12 dynamicData">
                    <div class="row">
                        <div class="col-md-4">Supplier Name :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->payee}}" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Amount :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->amount_numbers}} /-" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">No. of Days :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->no_of_days}}" disabled="" type="text"></div>
                    </div>
                     <div class="row">
                        <div class="col-md-4">Date :</div>
                        <div class="col-md-8"><input class="form-control" value="{{$request_data->bill_date}}" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">City :</div>
                        <div class="col-md-8"><textarea disabled="" type="text" class="form-control">{{$request_data->supplier_place}}</textarea></div>
                    </div>
                    <div class="col-md-12"><hr></div>
                
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="row">
                        <div class="col-md-4">Amount :</div>
                        <div class="col-md-8"><input class="form-control bidBudget" name="bidBudget" id="bidBudget" type="text" placeholder="Amount" required="required"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Rate :</div>
                        <div class="col-md-8"><input class="form-control bidBudget" name="bidRate" id="bidRate" type="text" placeholder="Rate" required="required"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Date :</div>
                        <div class="col-md-8"><input class="form-control bidBudget" name="bidDate" id="bidDate" type="date" required="required" ></div>
                        <input class="form-control bidBudget" name="req_id" id="req_id" type="hidden" value="{{$request_data->id}}" >
                        <input class="form-control bidBudget" name="supplier_id" id="supplier_id" type="hidden" value="{{$request_data->user_id}}" >
                        <input class="form-control bidBudget" name="supplier_amount" id="supplier_amount" type="hidden" value="{{$request_data->amount_numbers}}" > 
                    </div> 
                
                <div class="modal-footer"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                      @if(count($bid_data) > 0)
                      <input type="submit" id="placeBid" disabled="disabled" class="btn btn-success" value="Already Bided">
                      @else
                      <input type="submit" id="placeBid" class="btn btn-success" value="Bid">
                      @endif
                    <button type="button" class="btn btn-default btnCloseModal" data-dismiss="modal">Close</button>
                </div>
           
           </div> 
          </div>
        </div>
    </div>

</div>
 </form>
<div id="myModal" class="modal" style="padding-top:50px;">
      <span style="opacity: 1;" class="close">&times;</span>
       <img class="modal-content" id="img01">
        <object id="img02"  data="" type="application/pdf" width="100%" height="850">
            <p style="color: white;
        text-align: center;" class="err">
                It appears your Web browser is not configured to display PDF files. No worries, just <a class="downloadPdf" href="">click here to download the PDF file.</a>
            </p>
        </object>
    </div>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 12vh;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
<script>
var modal;
var modalImg;
var modalSrc;
$(document).ready(function(){
modal = document.getElementById('myModal');
modalImg = document.getElementById("img01");
modalSrc = document.getElementById("img02");
    $('.view').click(function(){
        var fileURL =$(this).attr('data-url');
        var ext = fileURL.substr(fileURL.lastIndexOf('.') + 1);
        $('#myModal').show();
        if(ext == 'pdf'){
            $('.err').show();
            document.getElementById("img01").src = '';
            document.getElementById("img02").data = fileURL+'#view=Fit';
            
            $('.downloadPdf').attr('href',fileURL);
            modalImg = '';
        }else{
             $('.err').hide();
            document.getElementById("img01").src = fileURL;
            document.getElementById("img02").data = '';
            modalSrc = '';
        }
        console.log(modalSrc,modalImg)    
        
        
    });
   

    $('.close').click(function(){
        console.log(1245)
        $('#myModal').hide();
        modalImg = '';
        modalSrc = '';
    });
});
</script>
@endsection