<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoicesign', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reqId');
            $table->string('userId');
            $table->string('signers');
            $table->string('signdone');
            $table->string('is_accepted');
            $table->string('totalsigners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoicesign');
    }
}
