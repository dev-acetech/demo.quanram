@extends('theme.default')


@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" style="visibility: hidden;">Notifications</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php


$get_notification = DB::table('notifications')->where('banker_id','=',Auth::user()->id)->orWhere('banker_id', '=', 'all')->orderBy('id', 'desc')->get();

$all_notifications = $get_notification;
$accepted_notifications = [];
$declined_notification = [];

if($supplier_all_notifications = DB::table('notifications')->where('supplier_id','=',Auth::user()->id)->orderBy('id', 'desc')->get());
$supplier_accepted_notifications = [];
$supplier_declined_notification = [];


$i = 0;
foreach ($get_notification as $k => $v) {
    if($v->is_type == 'accepted'){
        $accepted_notifications[$i] = $v;
        $i++;
    }else if($v->is_type == 'declined'){
        $declined_notification[$i] = $v;
        $i++;
    }
}

$j = 0;
foreach ($supplier_all_notifications as $k => $v) {
    if($v->is_type == 'accepted'){
        $supplier_accepted_notifications[$j] = $v;
        $j++;
    }else if($v->is_type == 'declined'){
        $supplier_declined_notification[$j] = $v;
        $j++;
    }
}

if(Auth::user()->role == 'supplier'){
$updateNotification =  DB::table('notifications')
                    ->where('supplier_id', Auth::user()->id)
                    ->update(['is_supplier_read' => 1]);
}
else if(Auth::user()->role == 'bank'){
 if($updateNotification =  DB::table('notifications')
                    ->where('banker_id','=', Auth::user()->id) 
                    ->where('banker_id', '!=', '007')
                     //->where('banker_id'=> Auth::user()->id, 'banker_id' != 'all')
                     ->update(['is_bank_read' => 1]));
}


 ?>

 @if(Auth::user()->role == 'supplier')
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-header card-header-tabs card-header-primary">
                                <div class="nav-tabs-navigation">
                                    <div class="nav-tabs-wrapper">
                                        <span class="nav-tabs-title">Notifications:</span>
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#All" data-toggle="tab">
                                                    <i class="material-icons">bug_report</i> All
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#Accepted" data-toggle="tab">
                                                    <i class="material-icons">code</i> Accepted
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#Declined" data-toggle="tab">
                                                    <i class="material-icons">cloud</i> Declined
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="All">
                                        <table class="table">
                                            <tbody>
                                                @if(count($supplier_all_notifications) > 0)
                                                @foreach ($supplier_all_notifications as $k => $v)
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    @if($v->is_supplier_read != 0)
                                                                    <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                    @else
                                                                    <input class="form-check-input" disabled type="checkbox" value="">
                                                                    @endif
                                                                    
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                    </tr>    
                                                @endforeach
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif                                                   
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="Accepted">
                                        <table class="table">
                                            <tbody>
                                               @if(count($supplier_accepted_notifications) > 0)
                                               @foreach ($supplier_accepted_notifications as $k => $v)
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    @if($v->is_supplier_read != 0)
                                                                    <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                    @else
                                                                    <input class="form-check-input" disabled type="checkbox" value="">
                                                                    @endif
                                                                    
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                    </tr>    
                                                @endforeach 
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif                                                  
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="Declined">
                                        <table class="table">
                                            <tbody>
                                                @if(count($supplier_declined_notification) > 0)
                                                @foreach ($supplier_declined_notification as $k => $v)
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                                @if($v->is_supplier_read != 0)
                                                                <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                @else
                                                                <input class="form-check-input" disabled type="checkbox" value="">
                                                                @endif
                                                                
                                                                <span class="form-check-sign">
                                                                    <span class="check"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                </tr>    
                                                @endforeach
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>
            </div>
        </div>
@else
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Notifications:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#All" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> All
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#Accepted" data-toggle="tab">
                                                        <i class="material-icons">code</i> Accepted
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#Declined" data-toggle="tab">
                                                        <i class="material-icons">cloud</i> Declined
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="All">
                                            <table class="table">
                                                <tbody>
                                                  @if(count($get_notification) > 0)
                                                  @foreach ($get_notification as $k => $v)
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    @if($v->is_supplier_read != 0)
                                                                    <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                    @else
                                                                    <input class="form-check-input" disabled type="checkbox" value="">
                                                                    @endif
                                                                    
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                    </tr>    
                                                @endforeach   
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif                                               
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="Accepted">
                                            <table class="table">
                                                <tbody>
                                                @if(count($accepted_notifications) > 0)
                                                @foreach ($accepted_notifications as $k => $v)
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    @if($v->is_supplier_read != 0)
                                                                    <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                    @else
                                                                    <input class="form-check-input" disabled type="checkbox" value="">
                                                                    @endif
                                                                    
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                    </tr>    
                                                @endforeach 
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="Declined">
                                            <table class="table">
                                                <tbody>
                                                @if(count($declined_notification) > 0)
                                                @foreach ($declined_notification as $k => $v)
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    @if($v->is_supplier_read != 0)
                                                                    <input class="form-check-input" disabled type="checkbox" value="" checked>
                                                                    @else
                                                                    <input class="form-check-input" disabled type="checkbox" value="">
                                                                    @endif
                                                                    
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td><a href="/request_view/{{$v->req_id}}">{{$v->notification_text}}</a></td>
                                                    </tr>    
                                                @endforeach 
                                                @else
                                                    <tr>
                                                        <td>No notifications available</td>
                                                    </tr>
                                                @endif                                               
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
@endif


      <script>
        $(document).ready(function(){
            //function getMessage(){
                $.ajax({
                   type:'POST',
                   url:'/updateNotification',
                   data:'_token = <?php echo csrf_token() ?>',
                   success:function(data){
                      //$("#msg").html(data.msg);
                      alert('success');
                   }
                });
             //}
        });
         
         </script>
@endsection