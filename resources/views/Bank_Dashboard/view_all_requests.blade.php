
@extends('theme.default')


@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Invoice View</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>
<?php 
    $all_requests = DB::table('request')->where('is_posted','=',1
                                                  )->get(); 
?>
<!-- /.row -->

<!------ Include the above in your HEAD tag ---------->

<div class="well ">
<?php if(count($all_requests) > 0){ ?>
<?php foreach ($all_requests as $k) { 
  $req_status = DB::table('placedbid')->where(['req_id'=>$k->id,'user_id'=>Auth::user()->id])->first();
// dd($req_status->status);
  ?>
<div class="row">
     <div class="col-lg-6 col-md-12">
         @if(!empty($req_status) && $req_status->status == 'accepted')
        <div class="panel panel-primary">
         @elseif(!empty($req_status) && $req_status->status == 'declined')
         <div class="panel panel-red">
         @else
         <div class="panel panel-green">
         @endif
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-4">
                        <p>Name : {{$k->supplier_fname}}</p>
                        <p>Address : {{$k->supplier_place}}</p>
                        <p>Amount : Rs.{{$k->amount_numbers}}</p>
                        <p>Days : {{$k->no_of_days}}</p>
                        <p>Date of Payment : {{$k->date_of_payment}}</p>
                    </div>
                    <div class="col-xs-6 text-right">
                      @if(($req_status))
                      <div class="huge">{{$req_status->status}}</div>
                      @else
                      <div class="huge">New</div>
                      @endif
                    </div>
                </div>
            </div>
            <a href="/view/{{$k->id}}">
                <div class="panel-footer">
                    <span class="pull-left"><a href="/view/{{$k->id}}">Bill of Exchange</a></span>
                    <span class=""><a href="#" type="button"  data-toggle="modal" data-target="#exampleModal{{$k->id}}" >&nbsp;&nbsp;&nbsp;View Buyer</a></span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal{{$k->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>Buyer's Details</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-xs-6">
                <p>Name : {{$k->buyer_name}}</p>
                <p>Email : {{$k->buyers_email}}</p>
                <p>Address : {{$k->buyer_address}}</p>
            </div>
             <div class="col-xs-6">
                <p>Bill No. : {{$k->bill_no}}</p>
                <p>Bill Date : {{$k->bill_date}}</p>
                <p>City : {{$k->buyers_city}}</p>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>    
<?php } else{ ?>
<div> Sorry no bills available</div>
<?php } ?>



@endsection
