<?php

namespace App\Http\Controllers;
use auth;

use Illuminate\Http\Request;
use Mail;
use DB;
use URL;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        if(Auth::user()->role == 'supplier'){
            return view('/Supplier_Dashboard/dashboard');
        }else if(Auth::user()->role == 'bank'){
            return view('/Bank_Dashboard/dashboard');
        }else {
            return view('/admin/dashboard');
        }
    }

    public function sendMail()
    {
        $buyerSignKey ='asdasdasd5a4sd5as5dasd4as4d4sad5s';
      $buyer_sign_data['receiver_name'] = 'testusrer';
      $buyer_sign_data['receiver_sign'] = URL::to('/').'/buyer-signature/'.$buyerSignKey;
       Mail::send('emails.buyer_sign', $buyer_sign_data, function($message) {
                $message->to('amitprithyani31@gmail.com')->subject('You have a New Invoice to sign');
            });
        
            $data['title'] = "Test it from HDTutu.com";
            $data['receiver_name'] = 'receiver_name';
                    // For Supplier
                        
            //         // For Receiver
            // Mail::send('emails.email', $data, function($message) {

            //     $message->to('amitprithyani31@gmail.com', 'Amit')

            //             ->subject('HDTuto.com Mail');

            // });
   dd("Mail Sent successfully");
    }
   


    /**

     * Show the my users page.

     *

     * @return \Illuminate\Http\Response

     */

    public function myUsers()

    {

        return view('myUsers');

    }



    public function buyerSignature($id){
       
   

       
        $checkBuyer = DB::table('buyersign')->where(['signature_number' => $id]
                                                 )->first();
        if($checkBuyer){
            DB::table('request')->where(['id' => $checkBuyer->request_id]
                                                 )->update(['is_invite_sent' => 1 ]);
            DB::table('buyersign')->where(['signature_number' => $id]
                                                 )->update(['buyer_sign' => 1 ]);
            $getBuyerData = DB::table('request')->where('id',$checkBuyer->request_id)->first();
            $buyer_name = $getBuyerData->buyer_name;
            $buyer_email = $getBuyerData->buyers_email;
            return view('Supplier_Dashboard.buyer_sign_done', ['buyer_name'=>$buyer_name,'buyer_email'=>$buyer_email]);
        }
        else{
            return view('Supplier_Dashboard.buyer_sign_error');
        }
        
    }
}
