@extends('theme.default')


@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">All Users</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php 
    $all_users = DB::table('users')->where('id','!=', Auth::user()->id)->get(); 
    //print_r($all_users);
?>

<div class="content">
   <div class="col-lg-10 col-md-12">
                <div class="card">
                    <div class="card-header card-header-warning">
                        <h4 class="card-title">Users Stats</h4>
                        <p class="card-category">All users</p>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover">
                            <thead class="text-warning">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>City</th>
                            </thead>
                            <tbody>
                                
                        <?php 
                        if(count($all_users) > 0){
                            $i = 1;
                             foreach ($all_users as $k => $v) { ?>
                            <tr>
                                <td>{{$i}}</td>
                                <td><?php echo  $v->name ?></td>
                                <td><?php echo $v->role ?></td>
                                <td><?php echo $v->city ?></td>
                            </tr>
                      <?php $i++; } }else{ ?>
                              <tr>
                                  <td colspan="4">No Users are Available</td>
                              </tr>
                      <?php  }  ?>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
   
</div>
@endsection