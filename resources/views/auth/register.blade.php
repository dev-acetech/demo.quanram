@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
              <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">Sign Up</h4>
                                    <p class="card-category">Create/Send Invoice</p>
                                </div>
                                <div class="card-body">
                                     <form method="POST" action="{{ route('register') }}">
                                             @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Name') }}</label>
                                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('E-Mail Address') }}</label>
                                                     <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Password') }}</label>
                                                   <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">{{ __('Confirm Password') }}</label>
                                                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Role</label>
                                                
                                                    <select name="role" >
                                                        <option value="supplier">Supplier</option>
                                                        <option value="bank">Bank</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="col-md-6 supplier_mobile">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Mobile Number</label>
                                                    <input id="mobNo" type="text" class="form-control" name="mobNo" >
                                                </div>
                                            </div>      
                                            <div class="col-md-6 supplier_gstno_block">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">GSTIN No.</label>
                                                    <input id="gstin" type="text" class="form-control" name="gst_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 supplier_tin_no_block">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">TIN No.</label>
                                                    <input id="tin" type="text" class="form-control" name="tin_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 supplier_tan_no_block">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">TAN No.</label>
                                                    <input id="tan" type="text" class="form-control" name="tan_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-6 supplier_pan_no_block">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">PAN No.</label>
                                                    <input id="pan" type="text" class="form-control" name="pan_no" >
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Company Aadhar</label>
                                                    <input id="company_adhar" type="text" class="form-control" name="com_aadhar">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">City</label>
                                                    <input id="city" type="text" class="form-control" name="city">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                            </div>
                                        </div>                                       
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
           
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue == 'supplier'){
                $('.supplier_gstno_block').show();
                $('.supplier_tin_no_block').show();
                $('.supplier_tan_no_block').show();
                $('.supplier_pan_no_block').show();
            }else if(optionValue == 'bank'){
                $('.supplier_gstno_block').hide();
                $('.supplier_tin_no_block').hide();
                $('.supplier_tan_no_block').hide();
                $('.supplier_pan_no_block').hide();
            }
        });
    }).change();
});
</script>
@endsection
