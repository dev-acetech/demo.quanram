<style type="text/css">
	table {
		  font-family: arial, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		  
		}
		.wall{
		padding-right: 15px;
		padding-left: 15px;
		min-height: 20px;
		padding: 19px;
		    padding-right: 19px;
		    padding-left: 19px;
		padding-right: 19px;
		padding-left: 19px;
		margin-bottom: 20px;
		background-color: white;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
		width: 600px;
		margin:0 auto;
		}
		td, th {
		  padding: 8px;
		}

		tr:nth-child(even) {
		 // background-color: #dddddd;
		}
		.center{
		    text-align: center;
		}
		.left{
		    text-align: left;
		}
		.right{
		    text-align: right;
		}
		.highlight{
		    color:brown;
		    font-weight: 600;
		}
</style>
<p>Hi, {{ $receiver_name }}</p>

<h1>You have a new Invoice to sign, click on below link to sign.</h1>
<br>
<div class="wall" style="padding-right: 15px;
		padding-left: 15px;
		min-height: 20px;
		padding: 19px;
		    padding-right: 19px;
		    padding-left: 19px;
		padding-right: 19px;
		padding-left: 19px;
		margin-bottom: 20px;
		background-color: white;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
		box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
		width: 420px;
		margin:0 auto;">
	<table>
		<tbody>
			<tr style="text-align:center">
				<th colspan="3" class="highlight" style="color:brown;
		    font-weight: 600;">Bill of Exchange</th>
			</tr>
			<tr>
				<td>{{$supplier_place}}, {{date('d-m-Y')}}</td>
				<td><span class="highlight" style="color:brown;
		    font-weight: 600;">Amount</span> IN Rs.{{$amount_numbers}}/-</td>
				<td><span class="highlight" style="color:brown;
		    font-weight: 600;">At </span> {{$no_of_days}} days after sight</td>
			</tr>
			<tr>
				<td  colspan="3"><span class="highlight" style="margin-right:25px;color:brown;
		    font-weight: 600;">Pay against this Sole Bill of Exchange to the order of</span> Ourselves</td>
			</tr>
			<tr>
				<td colspan="3"><span class="highlight" style="margin-right:25px;color:brown;
		    font-weight: 600;">the sum of </span> in Rs. {{$amount_words}} </td>

			</tr>
	        <tr>
				<td ><span class="highlight" style="margin-right:25px;color:brown;
		    font-weight: 600;">for value </span> Received.</td>
			</tr>
			<tr>
				<td class="highlight" style="color:brown;
		    font-weight: 600;">To:</td>
	            <td colspan="2" class="highlight right" style="color:brown;
		    font-weight: 600;text-align: right">For and on behalf of::</td>
			</tr>
			
			<tr>
	        	<td class="left">
	        		<p style="margin: 0">{{$buyer_address}}</p>
                    <p style="margin: 0">{{$buyer_name}}</p>
                    <p style="margin: 0">{{$buyers_email}}</p>
	        	</td>
	        <td colspan="2" class="right" style="text-align: right;">
	        	<p style="margin: 0">{{$payee}}</p>
                <p style="margin: 0">{{$supplier_place}}</p>
	        </td>
			</tr>
		
			
		</tbody>
	</table>
	</div>
<br>
<br>
<a href="{{$receiver_sign}}">{{$receiver_sign}}</a>

@if($attachments != '')
<br>
<br>
<h3>See Attachments (if any):</h3>
<br>
<?php $i = 1; ?>
@foreach($attachments as $atch)
<a href="{{URL::to('/').$atch}}">Attachment {{$i}}</a><br>
<?php $i++; ?>
@endforeach
@endif
<p>Thank you</p>

<p>Regards,</p>
<p>Your Admin</p>
<p><a href="http://demo.Klathrate.com">www.klathrate.com</p>