<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use auth;
use Mail;
use PDF;
use Illuminate\Support\Facades\Hash;
use URL;
use File;
use Session;
use Redirect;
class dashboard extends Controller
{
    public function index()
    {
       
        if(Auth::user()->role == 'supplier'){
            return view('/Supplier_Dashboard/dashboard');
        }else if(Auth::user()->role == 'bank'){
            return view('/Supplier_Dashboard/dashboard');
        }else {
            return view('/admin/dashboard');
        }
    }

    public function createInvoice()
    {
        return view('/Supplier_Dashboard/create_request');

    }

     public function notifications()
    {
        return view('/Supplier_Dashboard/notifications');

    }

     public function supplier_Response()
    {
        return view('/Supplier_Dashboard/response');

    }

     public function userProfile()
    {
        return view('/user_profile');

    }

    public function dashboard()
    {
        return view('/Supplier_Dashboard/dashboard');

    }

    public function contact()
    {
        return view('/contact');

    }

    public function addInvoice(request $request)
    {
        $random_number = rand(10,10000);
        $storeData = array();
        $i = 0;
        if($request->hasfile('filename'))
         {
             $this->validate($request, [
                'filename' => 'required',
                'filename.*' => 'mimes:doc,pdf,docx,png,jpeg'
            ]);
            if (! File::exists(public_path()."/uploads")) {
                File::makeDirectory(public_path()."/uploads", 0775, true, true);
            }
            if (! File::exists(public_path()."/uploads/".Auth::user()->role.Auth::user()->id)) {
                File::makeDirectory(public_path()."/uploads/".Auth::user()->role.Auth::user()->id, 0775, true, true);
            }

            foreach($request->file('filename') as $file)
            {
                $name='_'.time().$file->getClientOriginalName();
                $file->move(public_path()."/uploads/".Auth::user()->role.Auth::user()->id.'/', $name);  
                $data[] = $name; 
                $syprepath = "/uploads/".Auth::user()->role.Auth::user()->id.'/'.$name; 

                $storeData[$i] = $syprepath;
                $i++;
                //print_r( $syprepath);
            }
         }
         
         if(count($storeData) <= 0){
            $storeData = ['NA'=>'NA'];
            $buyer_sign_data['attachments'] = array();
         }else{
            $buyer_sign_data['attachments'] = $storeData;
         }
         
       $store_data = DB::table('request')->insertGetId(
            [
             'request_id' => 'refer_id_for_req_id',
             'user_id' => Auth::user()->id,
             'payee'=>$request->payee, 

             'payer' => $request->payer, 
             'amount_numbers' => $request->amount_numbers, 
             'amount_words' => $request->amount_words, 

             'no_of_days' => $request->no_of_days, 
             'start_payment_date' => $request->start_payment_date, 
             'end_payment_date' => $request->end_payment_date,

             'supplier_email' =>  Auth::user()->email,
             'buyer_name' => $request->buyer_name, 
             'buyers_email' => $request->buyers_email,

             'buyer_address' => $request->buyer_address,
             'bill_no' => $request->bill_no, 
             'bill_date' => $request->bill_date,

             'buyers_place' => $request->buyers_place, 
             'supplier_place'=> $request->supplier_place,
             'is_posted' => 0,

             'status' => 0, 
             'invoice_type' => 'NA', 
             'attachments' => serialize($storeData),
             'is_signed' => 1,
             'otp' => $random_number,
             'is_invite_sent' => 0,
             'invoice_number' => $random_number
          ]
        );

       //is_signed = 1 means by default creator signed
       //is_invite_sent = 1 means invite default sent to buyer for sign via mail make 0 fol old process
        //send mail to buyer for sign via email on $request->buyers_email
       $buyerSignKey = md5(uniqid(rand(), true));
       $buyer_sign_request = DB::table('buyersign')->insertGetId(
            [
             'request_id' => $store_data,
             'user_id' => Auth::user()->id,
             'signature_number'=> $buyerSignKey,
             'buyer_email' => $request->buyers_email,
             'buyer_sign' => 0
             ]);

      $add_status = DB::table('status')->insert(
                [
                 'request_id' => $store_data, 
                 'status' => 'active'
              ]
            );

      $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $store_data,
             'notification_text' => 'Invoice number '. $random_number.' is Created',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' => '007',
             'supplier_id' => Auth::user()->id,
             'is_type' => 'new_created'
             ]);
      $buyer_sign_data['receiver_name'] = $request->buyer_name;
      $buyer_sign_data['receiver_sign'] = URL::to('/').'/buyer-signature/'.$buyerSignKey;
      $buyer_sign_data['buyers_email']= $request->buyers_email;
      $buyer_sign_data['supplier_place'] = $request->supplier_place;
      $buyer_sign_data['no_of_days'] = $request->no_of_days;
      $buyer_sign_data['amount_words'] = $request->amount_words;
      $buyer_sign_data['buyer_address']= $request->buyer_address;
      $buyer_sign_data['buyer_name']= $request->buyer_name;
      $buyer_sign_data['payee'] = $request->payee;
      $buyer_sign_data['amount_numbers'] = $request->amount_numbers;

       Mail::send('emails.buyer_sign', $buyer_sign_data, function($message) use($buyer_sign_data) {
                $message->to($buyer_sign_data['buyers_email'])->subject('You have a New Invoice to sign');
            });
      return redirect('/request_view/'.$store_data)->with('success','Invoice created successfully!');;
    }

    public function updateProfile(request $request){
        if( Auth::user()->role == "bank"){
            $update_profile = DB::table('users')->insert(
                [
                 'name' => $request->name, 
                 'email' => $request->email, 
                 'com_aadhar' => $request->com_adhaar, 
                 'city' => $request->city,
              ]
            );
        }else{
           $update_profile = DB::table('users')->insert(
                [
                 'name' => $request->name, 
                 'email' => $request->email, 
                 'gst_no' => $request->gst_no,
                 'tin_no' => $request->tin_no, 
                 'tan_no' => $request->tan_no,
                 'pan_no' => $request->pan_no,
                 'com_aadhar' => $request->com_adhaar, 
                 'city' => $request->city,
              ]
            ); 
        }

        return view('/user_profile')->with('success','Profile updated successfully!');
        
    }

    public function showRequestInvoice($id){
        $getAttch = DB::table('request')->where('id',$id)->first();
        $attchments = unserialize($getAttch->attachments);
        if(!empty($attchments['NA'])){
            $attchments = [];
           }
        return view('/Supplier_Dashboard/request_view')->with('attchments',$attchments);
    }

     public function postRequest($id)
    {
        if(Auth::user()->role == 'supplier'){
            $request_post =  DB::table('request')
                    ->where('id', $id)
                    ->update(['is_posted' => 1,'invoice_type' => '']);
            $get_bankers_data =  DB::table('users')
            ->where('role', 'bank')->get();
            $request_data =  DB::table('request')
                    ->where('id', $id)->first();
            $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $id,
             'notification_text' => 'Invoice number '. $request_data->invoice_number.' is Created',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' => '007',
             'supplier_id' => $request_data->user_id,
             'is_type' => 'new_created'
             ]);

            $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supplier_post', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Posted Succcessfully');
            });
            foreach ($get_bankers_data as $get_bankers_data){
               $data['receiver_name'] = $get_bankers_data->name;
             
                 Mail::send('emails.bank_get_post_request', $data, function($message) use($get_bankers_data) {
                    $message->to($get_bankers_data->email)->subject('You have a New Request');
                });
            }
            return view('/Supplier_Dashboard/dashboard')->with('successMsg','Invoice successfully Posted .');
        }

       
    }


    public function viewRequest()
    {
        return view('/Bank_Dashboard/view_all_requests');
    }


    public function view($id)
    {

      return redirect('/request_view/'.$id);
       // return view('/Bank_Dashboard/view_request');
    }

    public function viewQuotes($id)
    {
        echo "test";
        exit;
      return redirect('/Bank_Dashboard/view_bids/'.$id);
       // return view('/Bank_Dashboard/view_request');
    }

    /*Supplier all requests*/
    public function supplierAllRequests()
    {
        return view('/Supplier_Dashboard/all_requests');
    }

    public function bidType(request $request){
         $request_data = DB::table('request')->where(['id' => $request->reqId])->first();
         $get_bankers_data =  DB::table('users')
            ->where('role', 'bank')->get();

            if($request_data->attachments != ''){
                $addOn = unserialize($request_data->attachments);
                //$addOn = serialize($addOn);
            }else{
               $addOn = ['NA'=>'NA']; 
               //$addOn = serialize($addOn);
            }
       $count = count($addOn);
       if (! File::exists(public_path()."/uploads/".Auth::user()->role.Auth::user()->id)) {
             File::makeDirectory(public_path()."/uploads/".Auth::user()->role.Auth::user()->id, 0775, true, true);
         }

            // Set extra option
            PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif']);
            // pass view file
            $pdf = PDF::loadView('Supplier_Dashboard/dashboard');
          
            //SAVE PDF
           $pdf = PDF::loadView( 'pdfview', compact('somevar'))->save( public_path().'/uploads/'.Auth::user()->role.Auth::user()->id.'/invoice'.$request->reqId.'.pdf' ); 
           $pdf_path = '/uploads/'.Auth::user()->role.Auth::user()->id.'/invoice'.$request->reqId.'.pdf' ;
           //print_r($pdf_path);
           //DOWBLOAD PDF

          
          if(!empty($pdf_path)){
            $addOn[$count] = $pdf_path;
          }
          
        if($request->bidType == 'Open Bid'){
            $bidType = DB::table('request')->where(['id' => $request->reqId]
                                                 )->update(['invoice_type' => 'openBids' , 'is_posted' => 1, 'attachments' => serialize($addOn)]);
            
            $add_status = DB::table('status')->where(['request_id' => $request->reqId
                                                        ])->update(['status' => 'posted']);

            $store_notification = DB::table('notifications')->insertGetId(
                [
                 'req_id' => $request->reqId,
                 'notification_text' => 'Invoice number '. $request_data->invoice_number.' is Available',
                 'is_supplier_read'=> 0,
                 'is_bank_read' => 0,
                 'banker_id' => '007',
                 'supplier_id' => $request_data->user_id,
                 'is_type' => 'new_posted'
                 ]);

            
            foreach ($get_bankers_data as $get_bankers_data){
               $data['receiver_name'] = $get_bankers_data->name;
             
                 Mail::send('emails.bank_get_post_request', $data, function($message) use($get_bankers_data) {
                    $message->to($get_bankers_data->email)->subject('You have a New Invoice');
                    
                });
            }
            return redirect('/dashboard')->with('success', 'Invoice posted successfully!');


        }else if($request->bidType == 'Close Bid'){
            $bidType = DB::table('request')->where(['id' => $request->reqId]                                                     
                                                 )->update(['invoice_type' => 'closeBids' , 'is_posted' => 1, 'attachments' => serialize($addOn)]);
            $add_status = DB::table('status')->where(['request_id' => $request->reqId
                                                        ])->update(['status' => 'posted']);

            $store_notification = DB::table('notifications')->insertGetId(
                [
                 'req_id' => $request->reqId,
                 'notification_text' => 'Invoice number '. $request_data->invoice_number.' is Invoice',
                 'is_supplier_read'=> 0,
                 'is_bank_read' => 0,
                 'banker_id' => '007',
                 'supplier_id' => $request_data->user_id,
                 'is_type' => 'new_posted'
                 ]);

            $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supplier_post', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Posted Succcessfully');
                        
            });
            foreach ($get_bankers_data as $get_bankers_data){
               $data['receiver_name'] = $get_bankers_data->name;
             
                 Mail::send('emails.bank_get_post_request', $data, function($message) use($get_bankers_data) {
                    $message->to($get_bankers_data->email)->subject('You have a New Request');
                    
                });
            }
            return redirect('/dashboard')->with('success', 'Invoice posted successfully!');
        }
    }

      public function placeBid(request $request)
    {
        $store_data = DB::table('placedbid')->insertGetId(
            [
             'req_id' => $request->req_id,
             'bidBudget' => $request->bidBudget,
             'bidRate' =>$request->bidRate,
             'user_id' => Auth::user()->id,
             'bidDate'=> $request->bidDate,
             'bank_name'=>Auth::user()->name,
             'supplier_id'=> $request->supplier_id,
             'supplier_amount' => $request->supplier_amount,
             'status'=>'bidded'
             
          ]
        );
        $request_data =  DB::table('request')
                    ->where('id', $request->req_id)->first();
        $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $request->req_id,
             'notification_text' => 'Invoice number '. $request_data->invoice_number.' is Bidded',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' => Auth::user()->id,
             'supplier_id' => $request->supplier_id,
             'is_type' => 'bidded'
             ]);
        //Supplier Email notification
        $data['receiver_name'] = $request_data->payee;
            Mail::send('emails.email', $data, function($message) use($request_data) {
            $message->to($request_data->supplier_email, $request_data->payee)
                        ->subject('Invoice Bidded Succcessfully');
            });
        //Banker Email Notification
        $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.email', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Bidded Succcessfully');
            });

        return redirect('/dashboard')->with('success', 'Bid added successfully!'); 

        //return view('/Supplier_Dashboard/dashboard')->with('success','Bid added successfully!');
    }
    
      public function bankDecline($id)
    {
        $get_req_data =  DB::table('request')
            ->where('id', $id)->first(); 
        $sup_name = $get_req_data->payee;
        
        $accept_offer = DB::table('placedbid')->where(['req_id' => $id,
                                                     'user_id' => Auth::user()->id
                                                 ])->update(['status' => 'bank_declined']);
        $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $id,
             'notification_text' => 'Invoice number '. $get_req_data->invoice_number.' is Declined',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' =>  Auth::user()->id,
             'supplier_id' => $get_req_data->user_id,
             'is_type' => 'declined'
          ]
        );

        $add_status = DB::table('status')->where(['request_id' => $id
                                                        ])->update(['status' => 'bank_declined']);

       // TO Banker
       $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.bank_declined', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Declined Succcessfully');
            });

        // To Supplier
         $data['receiver_name'] = $get_req_data->payee;

            Mail::send('emails.bank_declined', $data, function($messages) {
            $messages->to($get_req_data->supplier_email, $sup_name)
                        ->subject('Invoice Declined');
            });
           

      return view('/Supplier_Dashboard/dashboard')->with('success','Bill declined successfully!');
    }

    public function supplierDecline($id)
    {   $get_req_data =  DB::table('request')
            ->where('id', $id)->first();
        $get_banker_data =  DB::table('placedbid')
            ->where('id', $id)->first(); 
        $get_bank_email =  DB::table('users')
            ->where('id', $get_banker_data->user_id)->first();  

        $accept_offer = DB::table('placedbid')->where(['req_id' => $id,
                                                     'supplier_id' => Auth::user()->id
                                                 ])->update(['status' => 'declined']);
        $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $id,
             'notification_text' => 'Invoice number '. $get_req_data->invoice_number.' is Declined',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' => $get_banker_data->user_id,
             'supplier_id' => $get_req_data->user_id,
             'is_type' => 'declined'
          ]
        );

        $add_status = DB::table('status')->where(['request_id' => $id
                                                        ])->update(['status' => 'sup_declined']);

       // TO supplier
       $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supplier_declined', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Declined Succcessfully');
            });

        // To Banker
       $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supplier_declined', $data, function($message) {
            $message->to($get_bank_email->email, $get_bank_email->name)
                        ->subject('Invoice Declined');
            });

      return view('/Supplier_Dashboard/dashboard')->with('success','Bill declined successfully!');
    }


    public function supplierAccept($id)
    {
       $get_req_data =  DB::table('request')
            ->where('id', $id)->first();
       $get_banker_data =  DB::table('placedbid')
            ->where('req_id', $id)->first();   
        $get_bank_email =  DB::table('users')
            ->where('id', $get_banker_data->user_id)->first();  
        
       $accept_offer = DB::table('placedbid')->where(['req_id' => $id,
                                                     'supplier_id' => Auth::user()->id
                                                 ])->update(['status' => 'accepted']);

       $store_notification = DB::table('notifications')->insertGetId(
            [
             'req_id' => $id,
             'notification_text' => 'Invoice number '. $get_req_data->invoice_number.' is Accepted',
             'is_supplier_read'=> 0,
             'is_bank_read' => 0,
             'banker_id' => $get_banker_data->user_id,
             'supplier_id' => $get_req_data->user_id,
             'is_type' => 'accepted'
          ]
        );
       $add_status = DB::table('status')->where(['request_id' => $id
                                                        ])->update(['status' => 'accepted']);
       // TO supplier
       $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supllier_accepted', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Accepted Succcessfully');
            });

        // To Banker
       $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supllier_accepted', $data, function($message) use($get_bank_email) {
            $message->to($get_bank_email->email, $get_bank_email->name)
                        ->subject('Invoice Accepted Succcessfully');
            });

      return view('/Supplier_Dashboard/dashboard')->with('success','Bill accepted successfully!');

    }

    public function hashGenerator($len = 15){
          $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
          $base = strlen($charset);
          $result = '';

          $now = explode(' ', microtime())[1];
          while ($now >= $base){
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
          }
          return $result;
        }

    public function sendInvites(Request $request){
            $getUserId = Auth::user()->id;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
            $getOTP = rand(10,10000);
            $OneSignalIdToSend = array();
            $signer = $request->signers;
               for($i=0; $i< count($signer); $i++){
              $addSigners = DB::table('invoicesign')->insertGetId(
                    [
                     'reqId' => $request->reqId,
                     'userId' => $getUsersData->id,
                     'signers'=> $signer[$i],
                     'signdone' => 0,
                     'is_accepted' => 0,
                     'totalsigners' => count($request->signers)
                     ]);
              $store_notification = DB::table('notifications')->insertGetId(
                [
                 'req_id' => $request->reqId,
                 'notification_text' => 'You have new Invoice to sign',
                 'is_supplier_read'=> 0,
                 'is_bank_read' => 0,
                 'banker_id' => '008',
                 'supplier_id' => $signer[$i],
                 'is_type' => 'sign_request'
              ]
            );
           if($getOneSignalId = DB::table('appusers')->where('userId',$signer[$i])->orderBy('id', 'desc')->first()){
               $OneSignalIdToSend[$i] = $getOneSignalId->onesignal_id;
               $random_number = rand(10,10000);
               $addSignersNotification = DB::table('appnotification')->insertGetId(
                        [
                         'reqId' => $request->reqId,
                         'from' => $getUsersData->id,
                         'to'=> $signer[$i],
                         'msg' => 'New Invoice to sign',
                         'is_read' => 0,
                        'otp' => $getOTP,
                        'notification_id' =>  $random_number
                         ]);
                  }
                  
                 $content = array(
                    "en" => 'You have one new Invoice to sign'
                    );
                $fields = array(
                        'app_id' => "c202f9af-b64d-467d-a6c9-dfb11b7cb3a3",
                        'include_player_ids' => $OneSignalIdToSend,
                        'data' => array("foo" => "bar"),
                        'contents' => $content
                    );
                
                $fields = json_encode($fields);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                           'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);
           }
           $addInvite = DB::table('request')->where(['id' => $request->reqId]
                                                 )->update(['is_invite_sent' => 1 ]);
                    
           
            
            //return $response;
           
         return redirect('/request_view/'.$request->reqId);            
                   

            
    }

    public function testAB(Request $request){
        print_r($request->signers);
        exit;
        return false;
        $store_data = DB::table('users')->insertGetId(
            [
                //'_token' => 'mYNwoExVYcOkKK2hsKWHdyktfFb3EkQKKvA0hFVg',
                'name' => 'amit',
                'email' => 'soms@mailinator.com',
                'password' => Hash::make(123456),
                'role' => 'supplier',
                'mobNo' => 986555,
                'otp' => rand(10,10000),
                'gst_no' => 212,
                'tin_no' => 1215,
                'tan_no' => 154 ,
                'pan_no' => 1245,
                'city' => 'ngp',
                'com_aadhar' => 1245
          ]
        );
          $keygen_api_key = md5(uniqid(rand(), true));
           $store_datas = DB::table('appusers')->insertGetId(
                    [
                     'userId' => $store_data,
                     'authKey'=>$keygen_api_key
                    ]
                );
          print_r($keygen_api_key);
          print_r($store_datas);
        
    }
    
     public function pdfview(Request $request)
    {

        $users = DB::table("users")->get();
        view()->share('users',$users);

        if($request->has('download')){

          
            if (! File::exists(public_path()."/uploads/".Auth::user()->role.Auth::user()->id)) {
                File::makeDirectory(public_path()."/uploads/".Auth::user()->role.Auth::user()->id.'/invoice_pdf', 0775, true, true);
            }

            // Set extra option
            PDF::setOptions(['dpi' => 100, 'defaultFont' => 'sans-serif']);
            // pass view file
           $pdf = PDF::loadView('Supplier_Dashboard/dashboard');
          
            //SAVE PDF
           $pdf = PDF::loadView( 'pdfview/1', compact('somevar'))->save( public_path().'/uploads/invoice_pdf/'.Auth::user()->role.Auth::user()->id.'.pdf' ); 
           $pdf_path = public_path().'/uploads/invoice_pdf/'.Auth::user()->role.Auth::user()->id.'.pdf' ;
           print_r($pdf_path);
           //DOWBLOAD PDF

           Mail::send('emails.emailpdf', [], function ($m) {
                $m->to('amitprithyani31@gmail.com');
                $m->subject('Your free PDF');
                $m->attach(public_path().'/uploads/invoice_pdf/'.Auth::user()->role.Auth::user()->id.'.pdf' );
            });
          
           print_r($pdf_path);
            //return $pdf->download('pdfview.pdf');
        }
        return view('/pdfview');
    }

     public function storefile(Request $request)

    {
         $storeData = ['NA'=>'NA'];
         print_r($storeData);
         exit;

       /* $this->validate($request, [

                'filename' => 'required',
                'filename.*' => 'mimes:doc,pdf,docx,zip'

        ]);*/
        
      // echo $request->texr;
 //print_r($request->filename);
       // exit;
        if($request->hasfile('filename'))
         {
             $this->validate($request, [
                'filename' => 'required',
                'filename.*' => 'mimes:doc,pdf,docx,png'
            ]);
            if (! File::exists(public_path()."/uploads")) {
                File::makeDirectory(public_path()."/uploads", 0775, true, true);
            }
            if (! File::exists(public_path()."/uploads/".Auth::user()->role.Auth::user()->id)) {
                File::makeDirectory(public_path()."/uploads/".Auth::user()->role.Auth::user()->id, 0775, true, true);
            }
            
           $storeData = array();
           $i = 0;
            foreach($request->file('filename') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path()."/uploads/".Auth::user()->role.Auth::user()->id.'/', $name);  
                $data[] = $name; 
                $syprepath = public_path()."/uploads/".Auth::user()->role.Auth::user()->id.'/'.$name; 

                $storeData[$i] = $syprepath;
                $i++;
                //print_r( $syprepath);
            }
         }
         
         $file->filename=json_encode($data);
         
        return back()->with('success', 'Your files has been successfully added');
    }

    //Updating Notifications via AJAX
    public function updateNotification(){
        echo "test";
        exit;
    }
    
    public function reminderMailBuyer(Request $request){

        $getRequestData = DB::table('request')->where('id',$request->reqId)->first();
        $getSIgnKey = DB::table('buyersign')->where('request_id',$request->reqId)->first();
        $attchments = [];
        $attchments  = unserialize($getRequestData->attachments);
       if(!empty($attchments['NA'])){
        $attchments = [];
       }
       
        $buyer_sign_data['attachments'] = $attchments;
        $buyer_sign_data['receiver_name'] = $getRequestData ->buyer_name;
        $buyer_sign_data['receiver_sign'] = URL::to('/').'/buyer-signature/'.$getSIgnKey->signature_number;
        $buyer_sign_data['buyers_email']= $getRequestData->buyers_email;
        $buyer_sign_data['supplier_place'] = $getRequestData->supplier_place;
        $buyer_sign_data['no_of_days'] = $getRequestData->no_of_days;
        $buyer_sign_data['amount_words'] = $getRequestData->amount_words;
        $buyer_sign_data['buyer_address']= $getRequestData->buyer_address;
        $buyer_sign_data['buyer_name']= $getRequestData->buyer_name;
        $buyer_sign_data['payee'] = $getRequestData->payee;
        $buyer_sign_data['amount_numbers'] = $getRequestData->amount_numbers;

       
         Mail::send('emails.buyer_sign', $buyer_sign_data, function($message) use($buyer_sign_data) {
                $message->to($buyer_sign_data['buyers_email'])->subject('Reminder - You have an Invoice to sign');
            });
        return back()->with('success', 'Reminder mail sent successfully');
    }

}

//Status STatus
// 0 Active
// 1 Accepted
// 2 bank decliend
// 3 supplier declined
