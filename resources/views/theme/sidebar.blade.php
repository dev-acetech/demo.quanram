<div class="sidebar" data-color="purple" data-background-color="white" data-image="/img/sidebar-1.jpg">
            <?php $supp_notifications = DB::table('notifications')->where(['is_supplier_read' => 0
                                                 , 'supplier_id' => Auth::user()->id ])->get();
                $bank_notifications = DB::table('notifications')->where(['is_bank_read' => 0
                                                 , 'banker_id' => Auth::user()->id ])->orwhere('banker_id','=', 'all')->get(); ?>

            <div class="logo">
                <a href="http://www.klathrate.com" class="simple-text logo-normal">
                    Invoices
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item ">
                        <a class="nav-link" href="/dashboard">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="/user-profile">
                            <i class="material-icons">person</i>
                            <p>My Profile</p>
                        </a>
                    </li>
                    @if(Auth::user()->role == 'supplier')
                    <li class="nav-item ">
                        <a class="nav-link" href="/create-invoice">
                            <i class="material-icons">content_paste</i>
                            <p>Create Invoice</p>
                        </a>
                     </li>
                    @elseif(Auth::user()->role == 'bank')
                     <li class="nav-item ">
                         <a class="nav-link" href="/all-invoices">
                            <i class="material-icons">content_paste</i>
                            <p>View Invoice</p>
                        </a>
                    </li>
                    @else
                    <li class="nav-item ">
                        <a class="nav-link" href="/all-available-invoices">
                            <i class="material-icons">content_paste</i>
                            <p>View All Invoices</p>
                        </a>
                    </li>
                     @endif
                   
                    @if(Auth::user()->role == 'supplier')
                    <li class="nav-item ">
                        <a class="nav-link" href="/notifications">
                            <i class="material-icons">notifications</i>
                            <p>Notifications ({{ count($supp_notifications) }})</p>
                         </a>
                    </li>
                    @elseif(Auth::user()->role == 'bank')
                     <li class="nav-item ">
                        <a class="nav-link" href="/notifications">
                            <i class="material-icons">notifications</i>
                            <p>Notifications ({{ count($bank_notifications) }})</p>
                         </a>
                    </li>
                   @endif
                            
                       
                    @if(Auth::user()->role == 'supplier')
                    <li class="nav-item ">
                        <a class="nav-link" href="/response">
                            <i class="material-icons">bubble_chart</i>
                            <p>Response</p>
                        </a>
                    </li>
                    @endif
                     @if(Auth::user()->role == 'supplier')
                     <li class="nav-item ">
                        <a class="nav-link" href="/all-invoices">
                            <i class="material-icons">library_books</i>
                            <p>My Invoices</p>
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->role == 'supplier')
                    <li class="nav-item ">
                        <a class="nav-link" href="/sign-invites">
                            <i class="material-icons">content_paste</i>
                            <p>Sign Invites</p>
                        </a>
                    </li>
                    @endif
                     @if(Auth::user()->role == 'admin')
                     <li class="nav-item">
                        <a class="nav-link" href="/all-users">
                            <i class="material-icons">people</i>
                            <p>All Users</p>
                        </a>
                    </li>
                    @endif
                    <li class="nav-item ">
                        <a class="nav-link" href="/contact">
                            <i class="material-icons">location_ons</i>
                            <p>Contact</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
    $(document).ready(function () {
        var url = window.location;
        $('ul.nav a[href="'+ url +'"]').parent().addClass('active');
        $('ul.nav a').filter(function() {
             return this.href == url;
        }).parent().addClass('active');
    });
</script> 