<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
    
  if($data['role'] == 'bank'){
                return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'role' => $data['role'],
                    'mobNo' =>$data['mobNo'],
                    'otp' => rand(10,10000),
                    'gst_no' =>'no_gst',
                    'tin_no' =>'no_tin',
                    'tan_no' =>'no_tan',
                    'pan_no' =>'no_pan',
                    'city' =>'city',
                    'com_aadhar' => $data['com_aadhar'],
                 ]); 
            }else{
               return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'role' => $data['role'],
                'mobNo' =>$data['mobNo'],
                'otp' => rand(10,10000),
                'gst_no' =>$data['gst_no'],
                'tin_no' =>$data['tin_no'],
                'tan_no' =>$data['tan_no'],
                'pan_no' =>$data['pan_no'],
                'city' =>$data['city'],
                'com_aadhar' => $data['com_aadhar'],
             ]); 
 
               $response = md5(uniqid(rand(), true));
               $store_data = DB::table('appusers')->insertGetId(
                    [
                     'userId' => Auth::user()->id,
                     'authKey'=>$keygen_api_key,
                     'device_type' => 'ab',
                     'device_token'=> 'ab',
                     'onesignal_id'=> 'ab',
                    ]
                );
               
               return response()->json($response);
            }
    }
}
