<!DOCTYPE html>
<html>
<head>
	<title>User list - PDF</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<style type="text/css">
	td{
		border-top:none  !important;
	}
	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th{
		padding: 5px !important;
	}
</style>

<?php  
//$id = \Request::segment(2);
//session()->put('id', 1);
$id = session()->get('id');

$request_data = DB::table('request')
                ->where('id','=',$id)
                ->first();

if($bid_data = DB::table('placedbid')
                ->where(['req_id'=>$id,'user_id'=>Auth::user()->id])
                ->get()){}
//print_r($request_data);
?>
<body>

	<table class="table " style="background-color: #f5f5f5;border: 1px solid #e3e3e3;width: 700px;">
		<tr>
			<th colspan="4" style="text-align: center;width: 100%;"><h3 style="color: brown;font-weight: 600;margin-top: 10px;"><b>Bill Of Exchange</b></h3></th>
		</tr>
		<tr>
			<td><label style="color: black;font-weight: bold;">Nagpur, {{$request_data->bill_date}} </label></td>
			<td><label style="color:brown;font-weight: bold;">Ammount</label> <label style="color: black;font-weight: bold;">IN Rs.{{$request_data->amount_numbers}}/-</label></td>
			<td colspan="2"><label style="color:brown;font-weight: bold;">At</label> <label style="color: black;font-weight: bold;">{{$request_data->no_of_days}} days after sight</label></td>
		</tr>
		<tr>
			<td colspan="3"><label style="color:brown;font-weight: bold;">Pay against this Sole Bill of Exchange to the order of</label></td>
			<td ><label style="color: black;font-weight: bold;">Ourselves</label></td>
		</tr>
		<tr>
			<td><label style="color:brown;font-weight: bold;">the sum of</label></td>
			<td colspan="3"><label style="color: black;font-weight: bold;">In Rs. {{$request_data->amount_words}}</label></td>
		</tr>
		<tr>
			<td><label style="color:brown;font-weight: bold;">for value</label></td>
			<td><label style="color: black;font-weight: bold;">Received</label></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2"><label style="color:brown;font-weight: bold;">To:</label></td>
			<td colspan="2"><label style="color:brown;font-weight: bold;">For and on behalf of:</label></td>
		</tr>
		<tr>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->buyer_name}}</label></td>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->payee}}</label></td>
		</tr>
		<tr>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->buyers_email}}</label></td>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->supplier_email}}</label></td>
		</tr>
		<tr>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->buyer_address}}</label></td>
			<td colspan="2"><label style="color: #333;font-weight: bold;">{{$request_data->supplier_place}}</label></td>
		</tr>
		</tbody>
	</table>

</body>
</html>