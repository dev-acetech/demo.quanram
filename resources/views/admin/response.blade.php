@extends('theme.default')


@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header" style="visibility: hidden;">Response</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>
<?php 
$request_data = DB::table('placedbid')
                ->where(['supplier_id' => Auth::user()->id, 'status' => 'bidded'])
                ->get();
                ?>

 @if(Auth::user()->role == 'supplier')
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Invoices:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#profile" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> Responce
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                 @if(count($request_data) > 0)
                                                <tbody>
                                                    <?php $i=0; foreach ($request_data as $k) { $i++; ?>
                                                   <tr>
                                                        <td>{{$i}}
                                                        </td>
                                                        <td>
                                                            <p><span class="text-warning"> Name :</span> {{Auth::user()->name}}</p>
                                                            <p><span class="text-warning">Provider Name :</span> {{$k->bank_name}}</p>
                                                            <p><span class="text-warning">Amount :</span> {{$k->supplier_amount}} /-</p>
                                                            <p><span class="text-warning">Offer Amount :</span> {{$k->bidBudget}} /-</p>
                                                            <p><span class="text-warning">Rate :</span> {{$k->bidRate}}%</p>
                                                            <p><span class="text-warning">Date of Payment :</span> {{$k->bidDate}}</p>
                                                        </td>
                                                       
                                                        <td class="td-actions text-right">
                                                            @if($k->status == 'accepted')
                                                            <a href="#" disabled><button type="button" rel="tooltip" title="Accepted" class="btn btn-primary btn-link btn-sm">
                                                                    <i class="material-icons">edit</i>
                                                                </button></a>
                                                            @else

                                                            <a href="/supplier_accept/{{$k->req_id}}"><button type="button" rel="tooltip" title="Accept" class="btn btn-primary btn-link btn-sm">
                                                                    <i class="material-icons">edit</i>
                                                                </button></a>
                                                            @endif
                                                            @if($k->status == 'declined')
                                                            <a href="#" disabled > <button type="button" rel="tooltip" title="Declined" class="btn btn-danger btn-link btn-sm">
                                                                    <i class="material-icons">close</i>
                                                                </button></a>
                                                            @else
                                                            <a href="/supplier_decline/{{$k->req_id}}"><button type="button" rel="tooltip" title="Decline" class="btn btn-danger btn-link btn-sm">
                                                                    <i class="material-icons">close</i>
                                                                </button></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                                @else
                                                <tr>
                                                    <td>No new response</td>
                                                </tr>
                                                @endif
                                            </table>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
@else
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Tasks:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#profile" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> Bugs
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="checkbox" value="" checked>
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="checkbox" value="">
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="checkbox" value="">
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                        </td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="form-check">
                                                                <label class="form-check-label">
                                                                    <input class="form-check-input" type="checkbox" value="" checked>
                                                                    <span class="form-check-sign">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Create 4 Invisible User Experiences you Never Knew About</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-link btn-sm">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
@endif
    
@endsection