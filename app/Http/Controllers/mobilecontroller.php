<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use auth;
use Mail;
use File;
use Storage;
use Illuminate\Support\Facades\Hash;

class mobilecontroller extends Controller
{
    public function myTest(Request $request){
        
    }

    public function loginAndSaveDeviceCreds(Request $request)
    {
        
        $response = ['status' => 'failed', 'title' => 'Error', 'message' => 'Error message', 'type' => 'error'];

        /*if($request->has('email') && $request->has('password') && $request->has('deviceType') && $request->has('deviceToken') && $request->has('oneSignalId'))*/
        if($request->has('email') && $request->has('password'))
        {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
            {
                $random_number = rand(100,10000);

                /*$user = User::where('email', $request->email)->first();
                $device = new DeviceId;
                $device->user_id = $user->id;
                $device->device_type = $request->deviceType;
                $device->device_token = $request->deviceToken;
                $device->onesignal_id = str_replace('"', '', $request->oneSignalId);
                $keygen_api_key = Keygen::alphanum(20)->generate('strtoupper');
                $device->api_key = $keygen_api_key;
                $device->save();*/
                $keygen_api_key = md5(uniqid(rand(), true));
		
                $store_data = DB::table('appusers')->insertGetId(
                    [
                     'userId' => Auth::user()->id,
                     'authKey'=>$keygen_api_key,
                     'password'=> $request->password,
                     'device_type' => $request->deviceType,
                     'device_token'=> $request->deviceToken,
                     'onesignal_id'=> str_replace('"', '', $request->oneSignalId)
                    ]
                );
                if($store_data){
                    $response = ['status' => 'success', 'token' => $keygen_api_key , 'otp' => $random_number];
                }else{
                    $response = ['status' => 'success', 'token' => 'key_not_generated'];
                }
            }
            else
            {
                $response = ['status' => 'failed'];
            }
        }

        return response()->json($response);
    }

    public function hashGenerator($len = 15){
          $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
          $base = strlen($charset);
          $result = '';

          $now = explode(' ', microtime())[1];
          while ($now >= $base){
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
          }
          return $result;
        }

     public function fetchmyInvoices(Request $request)
    {
    
        $response = ['status' => 'failed', 'title' => 'Error', 'message' => 'Unable to fetch Invoices history', 'type' => 'error'];

        if($request->has('api_key'))
        {
            $getInvoice = [];
            $invoiceDetail = [];
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $reqSined = [];
             
            $reqUnsigned = [];
            $reqTotal = [];
            $getSigned = DB::table('invoicesign')->where(['signers'=> $getUserId, 'signdone' => 1])->get();
            $j = 0;
            $getInvoices = [];
           // $reqSignersName = [];
            foreach ($getSigned as $k => $v) {
               $fetchInvoice = DB::table('request')->where('id',$v->reqId)->first();
               $reqTotal = DB::table('invoicesign')->where('reqId',$v->reqId)->get();
               $reqSined = DB::table('invoicesign')->where(['reqId' => $v->reqId, 'signdone' => 1])->get();
               $reqUnsigned = DB::table('invoicesign')->where(['reqId' => $v->reqId, 'signdone' => 0])->get();
               $reqDeclined = DB::table('invoicesign')->where(['reqId' => $v->reqId, 'is_accepted' => 3])->get();
               $getInvoices[$j] = $fetchInvoice;
               $invoiceDetail[$j]['total'] = count($reqTotal);
               $invoiceDetail[$j]['signed'] = count($reqSined);
               $invoiceDetail[$j]['unsigned'] = count($reqUnsigned);
               $invoiceDetail[$j]['reqDeclined'] = count($reqDeclined);
             
              
               $ABSignIds = [];
               $ABsignersName = [];
               $ABdeclined = [];
               $ABDeclinersName = [];
               $a=0;
               foreach($reqSined as $ab){
               $ABSignIds[$a] = $ab->signers;
               $data = DB::table('users')->where('id','=',$ABSignIds[$a])->first();
               $ABsignersName[$a] = $data->name;
               $a++;
               }
               
               $b=0;
               foreach($reqDeclined as $bab){
               $ABdeclined[$b] = $bab->signers;
               $Bdata = DB::table('users')->where('id','=',$ABdeclined[$b])->first();
               $ABDeclinersName[$b] = $Bdata->name;
               $b++;
               }
              
               $getInvoices[$j]->total =  count($reqTotal);
               $getInvoices[$j]->signed =  count($reqSined);
               $getInvoices[$j]->unsigned =  count($reqUnsigned);
               $getInvoices[$j]->reqDeclined = count($reqDeclined);
               $getInvoices[$j]->signersName = $ABsignersName;
               $getInvoices[$j]->reqSignersName = $ABsignersName;
                $getInvoices[$j]->ABDeclinersName = $ABDeclinersName;
              
               
               $j++;
            }
            
              $i = 0;
            foreach($getInvoices as $k){
    		$getInvoices[$i]->newAttach = @unserialize($k->attachments);
    		$i++;
            }
			

            if(count($getInvoices) > 0){
                $response = ['status' => 'success', 'invoices' => $getInvoices];
                
            }else{
                $response = ['status' => 'success', 'invoices' => 'noInvoices'];
            }
        }

        return response()->json($response);
    }
    
     public function fetchUnsignedInvoices(Request $request)
    {
        $response = ['status' => 'failed', 'title' => 'Error', 'message' => 'Unable to fetch Invoices history', 'type' => 'error'];

        if($request->has('api_key'))
        {
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getInvoices = DB::table('request')->where(['is_signed' => 0, 'user_id' => $getUserId ])->get();
            $i = 0;
            foreach($getInvoices as $k){
    		$getInvoices[$i]->newAttach = @unserialize($k->attachments);
    		$i++;
            }
            
            if(count($getInvoices) > 0){
                $response = ['status' => 'success', 'invoices' => $getInvoices];
            }else{
                $response = ['status' => 'success', 'invoices' => 'noInvoices'];
            }
        }

        return response()->json($response);
    }

     public function fetchotherInvoices(Request $request)
    {
        $response = ['status' => 'failed', 'title' => 'Error', 'message' => 'Unable to fetch Invoices history', 'type' => 'error'];

        if($request->has('api_key'))
        {
            $hasMorePages = 'laterUse';
            $nextPageURL = 'laterUse';
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getInvoices =  DB::table('request')->where('user_id','!=', $getUserId)
            ->leftJoin('status', 'request.id', '=', 'status.request_id')
            ->get();
            if(count($getInvoices) > 0){
                $response = ['status' => 'success', 'invoices' => $getInvoices, 'has_more_pages' => $hasMorePages, 'next_page_url' => $nextPageURL];
            }else{
                $response = ['status' => 'success', 'invoices' => 'noInvoices', 'has_more_pages' => $hasMorePages, 'next_page_url' => $nextPageURL];
            }
        }

        return response()->json($response);
    }

    public function fetchSigners(Request $request)
    {
        $response = ['status' => 'failed', 'title' => 'Error', 'message' => 'Unable to fetch Signers history', 'type' => 'error'];

        if($request->has('api_key'))
        {   $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getSigners =  DB::table('users')->where('id','!=', $getUserId)->get();
            if(count($getSigners) > 0){
                $response = ['status' => 'success', 'signers' => $getSigners];
            }else{
                $response = ['status' => 'success', 'signers' => 'noSigners'];
            }
        }

        return response()->json($response);
    }

     public function fetchNotifications(Request $request)
    {

        if($request->has('api_key'))
        {    

            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;

           $getnotification = DB::table('appnotification')->where(['to' => $getUserId ,'is_read'=> 0])
           ->leftJoin('request', 'appnotification.reqId', '=', 'request.id')
           ->get();
	
		 $i = 0;
            foreach($getnotification as $k){
    		$getnotification[$i]->newAttach = @unserialize($k->attachments);
    		$i++;
            }

            
            $response = ['status' => 'success', 'notification' => $getnotification ];
            
        }

        return response()->json($response);
    }

     public function fetchUsersInfo(Request $request){
        if($request->has('api_key'))
        {
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
            if($getUsersData != ''){
                $response = ['status' => 'success', 'userData' => $getUsersData];
            }else{
                $response = ['status' => 'success', 'userData' => 'dataNotFound'];
            }
            
             return response()->json($response);
        }else{

            $response = ['status' => 'failed'];
            return response()->json($response);
        }
     }

     

     public function createInvoice(Request $request){
     
         if($request->has('api_key')){
          $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
            
                $random_number = rand(100,10000);
                $storeData = array();
                
                  
	            
                if($request->filename != '')
                 {
                  if (! File::exists(public_path()."/uploads")) {
                	File::makeDirectory(public_path()."/uploads", 0775, true, true);
	           }
	            if (! File::exists(public_path()."/uploads/supplier".$getUserId)) {
	                File::makeDirectory(public_path()."/uploads/supplier".$getUserId, 0775, true, true);
	            }
	        

	        //get the base-64 from data
	        $base64_str = substr($request->filename, strpos($request->filename, ",")+1);
	
	        //decode base64 string
	        $image = base64_decode($base64_str);
	       
	         $imageName = str_random(10).'.'.'png'; 
	        Storage::disk('local')->put("supplier".$getUserId.'/'.$imageName, $image);
	       
	        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
	        //echo $storagePath.$imageName;
	        
	        
                       
		      $storeData[0] = "supplier".$getUserId.'/'.$imageName;
        
                   
                 }
                 
                 if(count($storeData) <= 0){
                   $storeData = ['NA'=>'NA'];
                 }

           

               $store_data = DB::table('request')->insertGetId(
                    [
                     'request_id' => 'refer_id_for_req_id',
                     'user_id' => $getUsersData->id,
                     'payee'=>$request->payee, 

                     'payer' => $request->payer, 
                     'amount_numbers' => $request->amount_numbers, 
                     'amount_words' => $request->amount_words, 

                     'no_of_days' => $request->no_of_days, 
                     'start_payment_date' => $request->start_payment_date, 
                     'end_payment_date' => $request->end_payment_date,

                     'supplier_email' =>  $getUsersData->email,
                     'buyer_name' => $request->buyer_name, 
                     'buyers_email' => $request->buyers_email,

                     'buyer_address' => $request->buyer_address,
                     'bill_no' => $request->bill_no, 
                     'bill_date' => $request->bill_date,

                     'buyers_place' => $request->buyers_place, 
                     'supplier_place'=> $request->supplier_place,
                     'is_posted' => 0,

                     'status' => 0, 
                     'invoice_type' => 'NA', 
                     'attachments' => serialize($storeData),
                     'is_signed' => 0,
                     'is_invite_sent' => 0,
                     'otp' => $random_number,

                     'invoice_number' => $random_number
                  ]
                );
                
              $add_status = DB::table('status')->insert(
                        [
                         'request_id' => $store_data, 
                         'status' => 'active'
                      ]
                    );
            /*  $app_notification = DB::table('appnotification')->insertGetId(
                    [
                     'reqId' => $store_data,
                     'from' => $getUsersData->id,
                     'to'=> $getUsersData->id,
                     'msg' => 'Invoice number '. $random_number.' is Created',
                     'is_read' => 0,
                      'otp' => 00
                     ]);
                     */
              
              $store_notification = DB::table('notifications')->insertGetId(
                    [
                     'req_id' => $store_data,
                     'notification_text' => 'Invoice number '. $random_number.' is Created',
                     'is_supplier_read'=> 0,
                     'is_bank_read' => 0,
                     'banker_id' => '007',
                     'supplier_id' => $getUsersData->id,
                     'is_type' => 'new_created'
                     ]);
              if($store_data != ''){
                    $response = ['status' => 'success' , 'reqId' => $store_data, 'otp' => $random_number];
                    return response()->json($response);
              }else{
                    $response = ['status' => 'failed'];
                     return response()->json($response);
              }
              return redirect('/request_view/'.$store_data);
         }else{
            $response = ['status' => 'failed', 'reason' => 'noApiKey'];
             return response()->json($response);
         }
       
     }

      public function appRegistration(Request $request){
        $getOTP = rand(10,10000);
        $store_data = DB::table('users')->insertGetId(
            [

                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'role' => 'supplier',
                'mobNo' => $request->mobNo,
                'otp' => $getOTP,
                'gst_no' => $request->gst_no,
                'tin_no' => $request->tin_no,
                'tan_no' => $request->tan_no ,
                'pan_no' => $request->pan_no,
                'city' => $request->city,
                'com_aadhar' => $request->com_aadhar
          ]
        );
          $keygen_api_key = md5(uniqid(rand(), true));
           $store_datas = DB::table('appusers')->insertGetId(
                    [
                     'userId' => $store_data,
                     'authKey'=>$keygen_api_key,
                     'password' => $request->password,
                     'device_type' => $request->deviceType,
                     'device_token'=> $request->deviceToken,
                     'onesignal_id'=> str_replace('"', '', $request->oneSignalId)
                    ]
                );
           if($store_datas != ''){
            $response = ['status' => 'success','token' => $keygen_api_key , 'otp' => $getOTP];
           }else{
            $response = ['status' => 'failed','token' => 'noToken'];
           }
           
             return response()->json($response);
          //print_r($keygen_api_key);
          //print_r($store_datas);
        
    }

    public function addSigners(Request $request){
         if($request->has('api_key')){
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
              $getOTP = rand(10,10000);
            $OneSignalIdToSend = array();
           
            $signer =  $request->signers;
         
         $signer = explode(',', $request->signers);
          
               for($i=0; $i< count($signer); $i++){
              $addSigners = DB::table('invoicesign')->insertGetId(
                    [
                     'reqId' => $request->reqId,
                     'userId' => $getUsersData->id,
                     'signers'=> $signer[$i],
                     'signdone' => 0,
                     'is_accepted' => 0,
                     
                     'totalsigners' => count($request->signers)
                     
                     
                     ]);
                   $getOneSignalId = DB::table('appusers')->where('userId',$signer[$i])->orderBy('id', 'desc')->first();
                  
                   $OneSignalIdToSend[$i] = $getOneSignalId->onesignal_id;
                  
                    $random_number = rand(100,10000);
                $addSignersNotification = DB::table('appnotification')->insertGetId(
                    [
                     'reqId' => $request->reqId,
                     'from' => $getUsersData->id,
                     'to'=> $signer[$i],
                     'msg' => 'New Invoice to sign',
                     'is_read' => 0,
                    'otp' => $getOTP,
                    'notification_id' =>  $random_number
                     ]);
              }
               
               
          $addInvite = DB::table('request')->where(['id' => $request->reqId]
                                         )->update(['is_invite_sent' => 1 ]);
            
              $content = array(
			"en" => 'You have one new Invoice to sign'
			);
		
			$fields = array(
				'app_id' => "c202f9af-b64d-467d-a6c9-dfb11b7cb3a3",
				'include_player_ids' => $OneSignalIdToSend,
				'data' => array("foo" => "bar"),
				'contents' => $content
			);
		
			$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		//return $response;
		$responseBack = ['status' => 'success'];
                return response()->json($responseBack);
                 
               

         }
          $response = ['status' => 'failed'];
                return response()->json($response);
    }


    public function makeSign(Request $request){
         if($request->has('api_key')){
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
            $getReqData =  DB::table('request')->where('id',  $request->reqId)->first();

            $addSign = DB::table('invoicesign')->where(['signers' => $getUserId, 'reqId' => $request->reqId]
                                         )->update(['signdone' => 1, 'is_accepted' => 1 ]);
            $addSignersNotification = DB::table('appnotification')->where(['to' => $getUserId, 'reqId'=> $request->reqId]
                                         )->update(['is_read' => 1 ]);
	$text = 'Invoice number ' . $getReqData->invoice_number . ' is signed by ' . $getUsersData->name;
	$getOnesignalId = DB::table('appusers')->where('userId',$getReqData->user_id)->first();
	$random_number = rand(100,10000);
             $addNotification = DB::table('appnotification')->insertGetId(
                    [
                     'reqId' => $request->reqId,
                     'from' => $getUserId,
                     'to'=> $getReqData->user_id,
                     'msg' => $text,
                     'is_read' => 0,
                     'otp' => 0,
                     'notification_id' =>  $random_number
                     ]);
              
             $OneSignalIdToSend = DB::table('appusers')->where('userId','=',$getReqData->user_id)->orderBy('id', 'desc')->first();
            $recId[0] = $OneSignalIdToSend->onesignal_id;
              $content = array(
			"en" => $text
			);
		
			$fields = array(
				'app_id' => "c202f9af-b64d-467d-a6c9-dfb11b7cb3a3",
				'include_player_ids' => $recId[0],
				'data' => array("foo" => "bar"),
				'contents' => $content
			);
		
			$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$res = curl_exec($ch);
		curl_close($ch);
		
             $response = ['status' => 'success'];
                return [ response()->json($response)];
        }

        $response = ['status' => 'failed'];
            return response()->json($response);
    }
    
    public function invoiceDeclined(Request $request){
    	 if($request->has('api_key')){
    	   $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $getUsersData =  DB::table('users')->where('id', $getUserId)->first();
            $getReqData =  DB::table('request')->where('id',  $request->reqId)->first();
            $declineInvoice = DB::table('invoicesign')->where(['signers' => $getUserId, 'reqId' => $request->reqId]
                                         )->update(['signdone' => 3, 'is_accepted' => 3 ]);
            $updateNotification = DB::table('appnotification')->where(['to' => $getUserId, 'reqId'=> $request->reqId,'notification_id'=>$request->notification_id]
                                         )->update(['is_read' => 1 ]);
             $text = 'Invoice number ' . $getReqData->invoice_number . ' is declined by ' . $getUsersData->name;
             $random_number = rand(100,10000);
             $addNotification = DB::table('appnotification')->insertGetId(
	            [
	             'reqId' => $request->reqId,
	             'from' => $getUserId,
	             'to'=> $getReqData->user_id,
	             'msg' => $text,
	             'is_read' => 0,
	             'otp' => 0,
	             'notification_id' =>  $random_number
	             ]); 
	                                                                   
    	 	$response = ['status' => 'success'];
                return response()->json($response);
    	 }
    	 
    	  $response = ['status' => 'failed'];
            return response()->json($response);
    }
    
    
     public function creatorSign(Request $request){
         if($request->has('api_key')){
            $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
            $getUserId = $getUserData->userId;
            $addSign = DB::table('request')->where(['id' => $request->reqId]
                                         )->update(['is_signed' => 1 ]);
        $addSigners = DB::table('invoicesign')->insertGetId(
                    [
                     'reqId' => $request->reqId,
                     'userId' => $getUserId,
                     'signers'=> $getUserId,
                     'signdone' => 1,
                     'totalsigners' => 1,
                     'is_accepted' => 1
                     ]);
            
             $response = ['status' => 'success'];
                return response()->json($response);
        }

        $response = ['status' => 'failed'];
            return response()->json($response);
    }

    public function checkPassword(Request $request){
            if($request->has('api_key')){
                $getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();

                if($request->password == $getUserData->password){
                     $response = ['status' => 'success'];
                     return response()->json($response);
                   }else{
                    $response = ['status' => 'PassNotMatch'];
                    return response()->json($response);
                   }
            }else{
              $response = ['status' => 'failed'];
              return response()->json($response);
            }
    }

    public function uploadTest(Request $request){
         $random_number = rand(100,10000);
                $storeData = array();
                $i = 0;
                if($request->hasfile('fileName'))
                 {
                     $this->validate($request, [
                        'fileName' => 'required',
                        'fileName.*' => 'mimes:doc,pdf,docx,png,jpeg'
                    ]);
                    if (! File::exists(public_path()."/uploads")) {
                        File::makeDirectory(public_path()."/uploads", 0775, true, true);
                    }
                    if (! File::exists(public_path()."/uploads/abc")) {
                        File::makeDirectory(public_path()."/uploads/abc", 0775, true, true);
                    }

                    foreach($request->file('filename') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->move(public_path()."/uploads/abc".'/', $name);  
                        $data[] = $name; 
                        $syprepath = public_path()."/uploads/abc".'/'.$name; 

                        $storeData[$i] = $syprepath;
                        $i++;
                        //print_r( $syprepath);
                    }
                    $response = ['status' => $storeData];
              return response()->json($response);
                 }
                 $response = ['status' => 'failed'];
              return response()->json($response);
    }
    
     public function removeNotification(Request $request){
            if($request->has('api_key')){
               
		$getUserData = DB::table('appusers')->where('authKey' ,$request->api_key)->first();
                $getUserId = $getUserData->userId;
                $addSign = DB::table('appnotification')->where(['reqId' => $request->reqId, 'to' =>$getUserId]
                                         )->update(['is_read' => 1 ]);
                 $response = ['status' => 'success'];
                  return response()->json($response);
            }else{
              $response = ['status' => 'failed'];
              return response()->json($response);
            }
    }
    
   function sendMessage(){
		$content = array(
			"en" => 'English Message'
			);
		
		$fields = array(
			'app_id' => "c202f9af-b64d-467d-a6c9-dfb11b7cb3a3",
			'include_player_ids' => array("7acd7c94-79c2-46d2-a20f-002004b5e42c"),
			'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
	    	print("\nJSON sent:\n");
	    	print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
		
		 /*  $content = array(
			"en" => $text
			);
			$fields = array(
				'app_id' => "c202f9af-b64d-467d-a6c9-dfb11b7cb3a3",
				'include_player_ids' => [$getOnesignalId->onesignal_id],
				'data' => array("foo" => "bar"),
				'contents' => $content
			);
		
			$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$ab = curl_exec($ch);
		curl_close($ch);
		
		//return $ab;
		 return [$ab, response()->json($response)];
		*/
	}
	
	
    

}
