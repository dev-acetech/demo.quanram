<div class="navbar-default sidebar" role="navigation">

    <div class="sidebar-nav navbar-collapse">

        <ul class="nav" id="side-menu">

            <li>

                <a href="/home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>

            </li>

            <li>
           <?php  if(Auth::user()->role == 'supplier'){ ?>
            <a href="/create_request"><i class="fa fa-bar-chart-o fa-fw"></i> Create Requests</a>
            <?php }else{ ?>
                <a href="/view_request"><i class="fa fa-bar-chart-o fa-fw"></i> View Requests</a>
            <?php } ?>
            </li>

            <li>
                
                <a href="/notifications"><i class="fa fa-table fa-fw"></i> Notifications</a>

            </li>

            <li>
            <?php  if(Auth::user()->role == 'supplier'){ ?>
                <a href="/response"><i class="fa fa-edit fa-fw"></i> Response</a>
            <?php }else{ ?>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Dummy</a>
            <?php } ?>
                

            </li>

            <li>
            <?php  if(Auth::user()->role == 'supplier'){ ?>
                <a href="/supplier_all_request"><i class="fa fa-wrench fa-fw"></i> My Requests</a>
            <?php }else{ ?>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Dummy</a>
            <?php } ?>
               

            </li>


        </ul>

    </div>

    <!-- /.sidebar-collapse -->

</div>

<!-- /.navbar-static-side -->