@extends('theme.default')


@section('content')
<div class="row">
    <div class="col-lg-12">
        <!-- <h1 class="page-header">All Bill of Exchange</h1> -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php 
    $all_requests = DB::table('request')->get(); 
    $closeBids = [];
    $openBids = [];
    $bankOpenBids = [];
    $bankCloseBids = [];
    $i = 0;
    $j = 0;
    foreach ($all_requests as $k => $v) {
       // if($v->is_posted == 1){
            if($v->invoice_type == 'openBids'){
                $bankCloseBids[$i] = $v;
                $i++;
            }else if($v->invoice_type == 'closeBids'){
                $bankOpenBids[$i] = $v;
                $i++;
            } 
        //}
    }

    foreach ($all_requests as $k => $v) {
            if($v->invoice_type == 'openBids'){
                $closeBids[$j] = $v;
                $j++;
            }else if($v->invoice_type == 'closeBids'){
                $openBids[$j] = $v;
                $j++;
            } 
    }

?>

<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Invoices:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                @if(Auth::user()->role == 'supplier')
                                                 <li class="nav-item">
                                                    <a class="nav-link active" href="#All" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> All
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                @endif
                                                <li class="nav-item ">
                                                    @if(Auth::user()->role == 'supplier')
                                                    <a class="nav-link" href="#openBids" data-toggle="tab">
                                                    @else
                                                    <a class="nav-link active" href="#openBids" data-toggle="tab">
                                                    @endif
                                                        <i class="material-icons">code</i> Open Bids
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#closeBids" data-toggle="tab">
                                                        <i class="material-icons">cloud</i> CLose Bids
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        @if(Auth::user()->role == 'supplier')
                                        <div class="tab-pane active" id="All">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <thead class="text-warning">
                                                        <th>ID</th>
                                                        <th>Payee</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>                                                        
                                                        <th>Payer</th>
                                                        <th>Payer Email</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>                                                        
                                                        <th>Action</th>
                                                        <th>Status</th>
                                                    </thead>
                                                    <tbody>
                                                       <?php $i = 0; ?>
                                                        <?php 
                                                         $i++; if(count($all_requests) > 0) { 
                                                            foreach ( $all_requests as $k => $v) { ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$v->payee}}</td>
                                                            <td>{{$v->supplier_email}}</td>
                                                            <td>{{$v->amount_numbers}}</td>                                                            
                                                            <td>{{$v->payer}}</td>
                                                            <td>{{$v->buyers_email}}</td>
                                                            <td>{{$v->start_payment_date}}</td>
                                                            <td>{{$v->end_payment_date}}</td>
                                                            <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                             <td>
                                                            <?php $get_status = DB::table('status')->where('request_id','=',$v->id)->first();  ?>
                                                                @if($get_status->status == 'active')
                                                                <button class="btn btn-primary" disabled="disabled">Not Posted</button></td>
                                                                @elseif($get_status->status == 'posted')
                                                                <button class="btn btn-primary" disabled="disabled">Posted</button></td>
                                                                @elseif($get_status->status == 'accepted')
                                                                <button class="btn btn-primary" disabled="disabled">Accepted</button></td>
                                                                @elseif($get_status->status == 'sup_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Supplier Declined</button></td>
                                                                @elseif($get_status->status == 'bank_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Bank Declined</button></td>
                                                                @else
                                                                @endif
                                                        </tr>   
                                                        <?php $i++; } }else {?>
                                                        <tr>
                                                            <td colspan="10">No Invoices are Available</td>
                                                        </tr>    
                                                        <?php }?>
                                                    </tbody>
                                                </table>                                               
                                            </div>
                                        </div>
                                         @endif
                                        @if(Auth::user()->role == 'supplier')
                                         <div class="tab-pane" id="openBids">
                                        @else
                                         <div class="tab-pane active" id="openBids">
                                        @endif
                                       
                                            <div class="card-body table-responsive">
                                                @if(Auth::user()->role == 'supplier')
                                                    <table class="table table-hover">
                                                    <thead class="text-warning">
                                                        <th>ID</th>
                                                        <th>Payee</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>                                                        
                                                        <th>Payer</th>
                                                        <th>Payer Email</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>                                                        
                                                        <th>Action</th>
                                                        <th>Status</th>
                                                    </thead>
                                                    <tbody>
                                                       <?php $i = 0; ?>
                                                        <?php 
                                                         $i++; if(count($openBids) > 0) { 
                                                            foreach ( $openBids as $k => $v) { ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$v->payee}}</td>
                                                            <td>{{$v->supplier_email}}</td>
                                                            <td>{{$v->amount_numbers}}</td>                                                            
                                                            <td>{{$v->payer}}</td>
                                                            <td>{{$v->buyers_email}}</td>
                                                            <td>{{$v->start_payment_date}}</td>
                                                            <td>{{$v->end_payment_date}}</td>
                                                            <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                             <td>
                                                                <?php $get_status = DB::table('status')->where('request_id','=',$v->id)->first();  ?>
                                                                @if($get_status->status == 'active')
                                                                <button class="btn btn-primary" disabled="disabled">Not Posted</button></td>
                                                                @elseif($get_status->status == 'posted')
                                                                <button class="btn btn-primary" disabled="disabled">Posted</button></td>
                                                                @elseif($get_status->status == 'accepted')
                                                                <button class="btn btn-primary" disabled="disabled">Accepted</button></td>
                                                                @elseif($get_status->status == 'sup_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Supplier Declined</button></td>
                                                                @elseif($get_status->status == 'bank_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Bank Declined</button></td>
                                                                @else
                                                                @endif
                                                        </tr>   
                                                        <?php $i++; } }else {?>
                                                        <tr>
                                                            <td colspan="10">No Invoices are Available</td>                                                            
                                                        </tr>    
                                                        <?php }?>
                                                    </tbody>
                                                </table>
                                                @else
                                                    <table class="table table-hover">
                                                    <thead class="text-warning">
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>                                                        
                                                        <td>Payer</td>
                                                        <th>Action</th>
                                                        <th>Status</th>
                                                    </thead>
                                                    <tbody>
                                                       <?php $i = 0; if(count($bankOpenBids) > 0) {
                                                        foreach ( $bankOpenBids as $k => $v) {
                                                         $i++;  ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$v->payee}}</td>
                                                            <td>{{$v->supplier_email}}</td>
                                                            <td>{{$v->amount_numbers}}</td>                                                            
                                                            <td>{{$v->payer}}</td>                                                           
                                                            <td><a href="/viewQuotes/{{$v->id}}"><button class="btn btn-primary" type="button"  data-toggle="modal" data-target="#placeQuote" >View Quote</button></a><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a>
                                                                <?php $get_status = DB::table('status')->where('request_id','=',$v->id)->first();  ?>
                                                                @if($get_status->status == 'bank_declined')
                                                                <button class="btn btn-danger" disabled><a href="#"  style="color: white;">Rejected</a></button>
                                                                @else
                                                               <!--  <button class="btn btn-danger"><a href="/banker_decline/{{$v->id}}" style="color: white;">Reject</a></button> -->
                                                                @endif
                                                             <td>
                                                                @if($get_status->status == 'active')
                                                                <button class="btn btn-primary" disabled="disabled">Active</button></td>
                                                                @elseif($get_status->status == 'posted')
                                                                <button class="btn btn-primary" disabled="disabled">Available</button></td>
                                                                @elseif($get_status->status == 'accepted')
                                                                <button class="btn btn-primary" disabled="disabled">Sold Out</button></td>
                                                                @elseif($get_status->status == 'sup_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Supplier Declined</button></td>
                                                                @elseif($get_status->status == 'bank_declined')
                                                                <button class="btn btn-primary" disabled="disabled">Bank Declined</button></td>
                                                                @else
                                                                @endif
                                                        </tr>   
                                                        <?php $i++; } } else { ?>
                                                        <tr>
                                                            <td colspan="10">No Invoices are Available</td>                                                            
                                                        </tr>    
                                                        <?php }?>
                                                    </tbody>
                                                </table>
                                                @endif
                                                
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="closeBids">
                                            <div class="card-body table-responsive">
                                                 @if(Auth::user()->role == 'supplier')
                                                    <table class="table table-hover">
                                                        <thead class="text-warning">
                                                            <th>ID</th>
                                                            <th>Payee</th>
                                                            <th>Email</th>
                                                            <th>Amount</th>                                                        
                                                            <th>Payer</th>
                                                            <th>Payer Email</th>
                                                            <th>Start Date</th>
                                                            <th>End Date</th>                                                        
                                                            <th>Action</th>
                                                            <th>Status</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0;
                                                            
                                                             if(count($closeBids) > 0) { 
                                                                foreach ( $closeBids as $k => $v) {  $i++; ?>                                                        
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$v->payee}}</td>
                                                                <td>{{$v->supplier_email}}</td>
                                                                <td>{{$v->amount_numbers}}</td>                                                            
                                                                <td>{{$v->payer}}</td>
                                                                <td>{{$v->buyers_email}}</td>
                                                                <td>{{$v->start_payment_date}}</td>
                                                                <td>{{$v->end_payment_date}}</td>
                                                                <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                                 <td>
                                                                    <?php $get_status = DB::table('status')->where('request_id','=',$v->id)->first();  ?>
                                                                    @if($get_status->status == 'active')
                                                                    <button class="btn btn-primary" disabled="disabled">Not Posted</button></td>
                                                                    @elseif($get_status->status == 'posted')
                                                                    <button class="btn btn-primary" disabled="disabled">Posted</button></td>
                                                                    @elseif($get_status->status == 'accepted')
                                                                    <button class="btn btn-primary" disabled="disabled">Accepted</button></td>
                                                                    @elseif($get_status->status == 'sup_declined')
                                                                    <button class="btn btn-primary" disabled="disabled">Supplier Declined</button></td>
                                                                    @elseif($get_status->status == 'bank_declined')
                                                                    <button class="btn btn-primary" disabled="disabled">Bank Declined</button></td>
                                                                    @else
                                                                    @endif
                                                            </tr> 
                                                            <?php } } else { ?>
                                                            <tr>
                                                                <td colspan="10">No Invoices are Available</td>                                                            
                                                             </tr> 
                                                           <?php } ?>                                                         
                                                                                                            
                                                        </tbody>
                                                    </table>
                                                 @else
                                                    <table class="table table-hover">
                                                        <thead class="text-warning">
                                                            <th>ID</th>
                                                            <th>Names</th>
                                                            <th>Email</th>
                                                            <th>Amount</th>                                                        
                                                            <th>Payer</th>
                                                            <th>Action</th>
                                                            <th>Status</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0; if(count($bankCloseBids) > 0) {
                                                            foreach ( $bankCloseBids as $k => $v) {
                                                             $i++;  ?>                                                        
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$v->payee}}</td>
                                                                <td>{{$v->supplier_email}}</td>
                                                                <td>{{$v->amount_numbers}}</td>                                                            
                                                                <td>{{$v->payer}}</td>
                                                                <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a>
                                                                <?php $get_status = DB::table('status')->where('request_id','=',$v->id)->first();  ?>
                                                                @if($get_status->status == 'bank_declined')
                                                                <button class="btn btn-danger" disabled><a href="#"  style="color: white;">Rejected</a></button>
                                                                @else
                                                               <!--  <button class="btn btn-danger"><a href="/banker_decline/{{$v->id}}" style="color: white;">Reject</a></button> -->
                                                                @endif
                                                                    </td>
                                                                <td>
                                                                    
                                                                    @if($get_status->status == 'active')
                                                                    <button class="btn btn-primary" disabled="disabled">Active</button></td>
                                                                    @elseif($get_status->status == 'posted')
                                                                    <button class="btn btn-primary" disabled="disabled">Available</button></td>
                                                                    @elseif($get_status->status == 'accepted')
                                                                    <button class="btn btn-primary" disabled="disabled">Sold Out</button></td>
                                                                    @elseif($get_status->status == 'sup_declined')
                                                                    <button class="btn btn-primary" disabled="disabled">Supplier Declined</button></td>
                                                                    @elseif($get_status->status == 'bank_declined')
                                                                    <button class="btn btn-primary" disabled="disabled">Bank Declined</button></td>
                                                                    @else
                                                                    @endif
                                                            </tr> 
                                                            <?php } } else { ?>
                                                            <tr>
                                                                <td colspan="10">No Invoices are Available</td>                                                            
                                                             </tr> 
                                                           <?php  } ?>                                                         
                                                                                                            
                                                        </tbody>
                                                    </table>
                                                 @endif
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<div id="placeQuote" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Place Quote</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12 dynamicData">
                    <div class="row">
                        <div class="col-md-4">Supplier Name :</div>
                        <div class="col-md-8 "><input class="form-control" value="" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">Amount :</div>
                        <div class="col-md-8 "><input class="form-control" value=" /-" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">No. of Days :</div>
                        <div class="col-md-8 "><input class="form-control" value="" disabled="" type="text"></div>
                    </div>
                     <div class="row">
                        <div class="col-md-4">Date :</div>
                        <div class="col-md-8"><input class="form-control" value="" disabled="" type="text"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">City :</div>
                        <div class="col-md-8"><textarea disabled="" type="text" class="form-control"></textarea></div>
                    </div>
                    <!-- <div class="col-md-12"><hr></div> -->
                
            <form  method="post" action="/placeBid" enctype="multipart/form-data" name="placeBid">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="row">
                    <div class="col-md-4">Amount :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidBudget" id="bidBudget" type="text" placeholder="Amount" required="required"></div>
                </div>
                <div class="row">
                    <div class="col-md-4">Rate :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidRate" id="bidRate" type="text" placeholder="Rate" required="required"></div>
                </div>
                <div class="row">
                    <div class="col-md-4">Date :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidDate" id="bidDate" type="date" required="required" ></div>
                    <input class="form-control bidBudget" name="req_id" id="req_id" type="hidden" value="" >
                    <input class="form-control bidBudget" name="supplier_id" id="supplier_id" type="hidden" value="" >
                    <input class="form-control bidBudget" name="supplier_amount" id="supplier_amount" type="hidden" value="" > 
                </div> 
            </div> 
            <div class="modal-footer"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                 
                  <input type="button" id="placeBid" disabled="disabled" class="btn btn-success" value="Already Bided">
                  
                  <input type="submit" id="placeBid" class="btn btn-success" value="Place Quote">
                 
                <button type="button" class="btn btn-default btnCloseModal" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        </div>
    </div>
</div>
<div id="myModal4" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
   
</div>
@endsection