@extends('theme.default')


@section('content')
<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Sign Invites</h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php 
$getRequests = DB::table('invoicesign')
            ->leftJoin('request', 'invoicesign.reqId', '=', 'request.id')
            ->get();
//print_r($getRequests);
    $i = 0;
    $unsigned = [];
    $signed = [];
    $declined = [];
if(count($getRequests) > 0){
     foreach ($getRequests as $k => $v) {
        if($v->signdone == 0){
            $unsigned[$i] = $v;
            $i++;
        }elseif($v->signdone == 1){
            $signed[$i] = $v;
            $i++;
        }else{
            $declined[$i] = $v;
            $i++;
        }
    }
}
   
    $all_requests = DB::table('request')->get(); 
    $closeBids = [];
    $openBids = [];
    $bankOpenBids = [];
    $bankCloseBids = [];
   
    $j = 0;
    foreach ($all_requests as $k => $v) {
        if($v->is_posted == 1){
            if($v->invoice_type == 'openBids'){
                $bankCloseBids[$i] = $v;
                $i++;
            }else if($v->invoice_type == 'closeBids'){
                $bankOpenBids[$i] = $v;
                $i++;
            } 
        }
    }

    foreach ($all_requests as $k => $v) {
            if($v->invoice_type == 'openBids'){
                $closeBids[$j] = $v;
                $j++;
            }else if($v->invoice_type == 'closeBids'){
                $openBids[$j] = $v;
                $j++;
            } 
    }

?>

<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Invoices:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                 <li class="nav-item">
                                                    <a class="nav-link active" href="#All" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> All
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item ">
                                                    <a class="nav-link" href="#openBids" data-toggle="tab">
                                                        <i class="material-icons">code</i> Unsigned
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#closeBids" data-toggle="tab">
                                                        <i class="material-icons">cloud</i> Signed
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#declined" data-toggle="tab">
                                                        <i class="material-icons">cloud</i> Declined
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="All">
                                            <div class="card-body table-responsive">
                                                <table class="table table-hover">
                                                    <thead class="text-warning">
                                                        <th>ID</th>
                                                        <th>Payee</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>                                                        
                                                        <th>Payer</th>
                                                        <th>Payer Email</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>                                                        
                                                        <th colspan="2">Action/Status</th>
                                                    </thead>
                                                    <tbody>
                                                       <?php $i = 0; ?>
                                                        <?php 
                                                         $i++; if(count($getRequests) > 0) { 
                                                            foreach ( $getRequests as $k => $v) { ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$v->payee}}</td>
                                                            <td>{{$v->supplier_email}}</td>
                                                            <td>{{$v->amount_numbers}}</td>                                                            
                                                            <td>{{$v->payer}}</td>
                                                            <td>{{$v->buyers_email}}</td>
                                                            <td>{{$v->start_payment_date}}</td>
                                                            <td>{{$v->end_payment_date}}</td>
                                                            <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                             <td>
                                                                @if($v->signdone == 0)
                                                                <button class="btn btn-primary" type="button"  data-toggle="modal" data-target="#makeSign" >Make signed</button></td>
                                                                @elseif($v->signdone == 1)
                                                                <button class="btn btn-primary" disabled="disabled">Signed</button></td>
                                                                @else
                                                                <button class="btn btn-primary" disabled="disabled">Declined to sign</button></td>
                                                                @endif
                                                        </tr>   
                                                        <?php $i++; } }else {?>
                                                        <tr>
                                                            <td colspan="10">No Invoices are Available</td>
                                                        </tr>    
                                                        <?php }?>
                                                    </tbody>
                                                </table>                                               
                                            </div>
                                        </div>
                                         <div class="tab-pane" id="openBids">
                                            <div class="card-body table-responsive">
                                                    <table class="table table-hover">
                                                    <thead class="text-warning">
                                                        <th>ID</th>
                                                        <th>Payee</th>
                                                        <th>Email</th>
                                                        <th>Amount</th>                                                        
                                                        <th>Payer</th>
                                                        <th>Payer Email</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>                                                        
                                                        <th colspan="2">Action/Status</th>
                                                    </thead>
                                                    <tbody>
                                                       <?php $i = 0; ?>
                                                        <?php 
                                                         $i++; if(count($unsigned) > 0) { 
                                                            foreach ( $unsigned as $k => $v) { ?>
                                                        <tr>
                                                            <td>{{$i}}</td>
                                                            <td>{{$v->payee}}</td>
                                                            <td>{{$v->supplier_email}}</td>
                                                            <td>{{$v->amount_numbers}}</td>                                                            
                                                            <td>{{$v->payer}}</td>
                                                            <td>{{$v->buyers_email}}</td>
                                                            <td>{{$v->start_payment_date}}</td>
                                                            <td>{{$v->end_payment_date}}</td>
                                                            <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                             <td>
                                                                @if($v->signdone == 0)
                                                                 <button class="btn btn-primary" type="button"  data-toggle="modal" data-target="#makeSign" >Make signed</button></td>
                                                                @elseif($v->signdone == 1)
                                                                <button class="btn btn-primary" disabled="disabled">Signed</button></td>
                                                                @else
                                                                <button class="btn btn-primary" disabled="disabled">Declined to sign</button></td>
                                                                @endif
                                                        </tr>   
                                                        <?php $i++; } }else {?>
                                                        <tr>
                                                            <td colspan="10">No Invoices are Available</td>                                                            
                                                        </tr>    
                                                        <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="closeBids">
                                            <div class="card-body table-responsive">
                                                    <table class="table table-hover">
                                                        <thead class="text-warning">
                                                            <th>ID</th>
                                                            <th>Payee</th>
                                                            <th>Email</th>
                                                            <th>Amount</th>                                                        
                                                            <th>Payer</th>
                                                            <th>Payer Email</th>
                                                            <th>Start Date</th>
                                                            <th>End Date</th>                                                        
                                                            <th colspan="2">Action/Status</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0;
                                                            
                                                             if(count($signed) > 0) { 
                                                                foreach ( $signed as $k => $v) {  $i++; ?>                                                        
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$v->payee}}</td>
                                                                <td>{{$v->supplier_email}}</td>
                                                                <td>{{$v->amount_numbers}}</td>                                                            
                                                                <td>{{$v->payer}}</td>
                                                                <td>{{$v->buyers_email}}</td>
                                                                <td>{{$v->start_payment_date}}</td>
                                                                <td>{{$v->end_payment_date}}</td>
                                                                <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                                 <td>
                                                                    @if($v->signdone == 0)
                                                                    <button class="btn btn-primary" type="button"  data-toggle="modal" data-target="#makeSign" >Make signed</button></td>
                                                                    @elseif($v->signdone == 1)
                                                                    <button class="btn btn-primary" disabled="disabled">Signed</button></td>
                                                                    @else
                                                                    <button class="btn btn-primary" disabled="disabled">Declined to sign</button></td>
                                                                    @endif
                                                            </tr> 
                                                            <?php } } else { ?>
                                                            <tr>
                                                                <td colspan="10">No Invoices are Available</td>                                                            
                                                             </tr> 
                                                           <?php } ?>                                                         
                                                                                                            
                                                        </tbody>
                                                    </table>
                                            </div>
                                        </div>
                                         <div class="tab-pane" id="declined">
                                            <div class="card-body table-responsive">
                                                    <table class="table table-hover">
                                                        <thead class="text-warning">
                                                            <th>ID</th>
                                                            <th>Payee</th>
                                                            <th>Email</th>
                                                            <th>Amount</th>                                                        
                                                            <th>Payer</th>
                                                            <th>Payer Email</th>
                                                            <th>Start Date</th>
                                                            <th>End Date</th>                                                        
                                                            <th colspan="2">Action/Status</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php $i = 0;
                                                            
                                                             if(count($declined) > 0) { 
                                                                foreach ( $declined as $k => $v) {  $i++; ?>                                                        
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$v->payee}}</td>
                                                                <td>{{$v->supplier_email}}</td>
                                                                <td>{{$v->amount_numbers}}</td>                                                            
                                                                <td>{{$v->payer}}</td>
                                                                <td>{{$v->buyers_email}}</td>
                                                                <td>{{$v->start_payment_date}}</td>
                                                                <td>{{$v->end_payment_date}}</td>
                                                                <td><a style="color: white;" href="/request_view/{{$v->id}}"><button class="btn btn-success">View</button></a></td>
                                                                 <td>
                                                                    @if($v->signdone == 0)
                                                                    <button class="btn btn-primary" disabled="disabled">Make signed</button></td>
                                                                    @elseif($v->signdone == 1)
                                                                    <button class="btn btn-primary" disabled="disabled">Signed</button></td>
                                                                    @else
                                                                    <button class="btn btn-primary" disabled="disabled">Declined to sign</button></td>
                                                                    @endif
                                                            </tr> 
                                                            <?php } } else { ?>
                                                            <tr>
                                                                <td colspan="10">No Invoices are Available</td>                                                            
                                                             </tr> 
                                                           <?php } ?>                                                         
                                                                                                            
                                                        </tbody>
                                                    </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<div id="makeSign" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Make Sign</h4>
            </div>
            <div class="modal-body row">
                <p>Unable to sign, Adhaar E-sign not available.</p> 
            
        </div>
        </div>
    </div>
</div>
<div id="myModal4" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
   
</div>
@endsection