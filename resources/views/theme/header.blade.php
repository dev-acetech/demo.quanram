<!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-transparent ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <a class="navbar-brand" href="#pablo">
                        @if(Auth::user()->role == 'supplier')
                           <b>Supplier's Dashboard</b>
                        @elseif(Auth::user()->role == 'bank')
                          <b> Banker's Dashboard</b>
                        @elseif(Auth::user()->role == 'admin')
                          <b> Admin's Dashboard</b>
                        @endif
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <form class="navbar-form">
                            <div class="input-group no-border">
                                <input type="text" value="" class="form-control" placeholder="Search...">
                                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                    <i class="material-icons">search</i>
                                    <div class="ripple-container"></div>
                                </button>
                            </div>
                        </form>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <i class="material-icons">dashboard</i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Stats</span>
                                    </p>
                                </a>
                            </li>
                            <?php
                            $notifications = array();
                            if(Auth::user()->role == 'bank'){
                                $supplier_all_notifications = DB::table('notifications')->where(['banker_id'=>Auth::user()->id, 'is_bank_read' => 0])->orderBy('id', 'desc')->get();
                                $notifications = $supplier_all_notifications;
                            }else if(Auth::user()->role == 'supplier'){
                                $get_notification = DB::table('notifications')->where(['supplier_id'=>Auth::user()->id, 'is_supplier_read' => 0])->orWhere('banker_id', '=', 'all')->orderBy('id', 'desc')->get();
                                $notifications = $get_notification;
                               
                            }

                            
                            
                            
                             ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">{{count($notifications)}}</span>
                                    <p>
                                        <span class="d-lg-none d-md-block">Some Actions</span>
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                    <?php if(count($notifications) > 0){ 
                                        foreach ($notifications as $k => $v) { ?>
                                             <a class="dropdown-item" href="#">{{$v->notification_text}}</a>
                                      <?php  }
                                        }else{ ?>
                                        <a class="dropdown-item" href="#">No new nnotifications.</a>
                                  <?php  } ?>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo" id="accountDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p>
                                        <span class="d-lg-none d-md-block">Account</span>
                                    </p>
                                </a>
                                 <div class="dropdown-menu dropdown-menu-right" aria-labelledby="accountDropdownMenuLink">
                                    <a class="dropdown-item" href="#">Mike John responded to your email</a>
                                    <a class="dropdown-item" href="#">You have 5 new tasks</a>
                                    <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                                    <a class="dropdown-item" href="#">Another Notification</a>
                                    <a class="dropdown-item"href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i>
                                        {{ __('Logout') }}>Logout</a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->