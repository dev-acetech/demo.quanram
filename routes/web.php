<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('temp', function () {
    return view('newtheme');
});

Route::get('temp2', function () {
    return view('newtheme1');
});

Route::get('temp3', function () {
    return view('/Supplier_Dashboard/klathrate_profile');
});

Route::get('temp4', function () {
    return view('/Supplier_Dashboard/klathrate_my_invoices');
});

Route::get('temp5', function () {
    return view('/Supplier_Dashboard/klathrate_notifications');
});

Route::get('temp6', function () {
    return view('/Supplier_Dashboard/klathrate_invoices');
});

Auth::routes();


Route::get('send-main', 'HomeController@sendMail');

Route::get('request_view/{id}', 'dashboard@showRequestInvoice')->name('request_view');


// Route::get('request_view/{id}', function () {
//     return view('/Supplier_Dashboard/request_view');
// });

Route::get('viewQuotes/{id}', function () {
    return view('/Bank_Dashboard/view_bids');
});

Route::get('/Supplier_Dashboard/request_view/{id}', function () {
    return view('/Supplier_Dashboard/request_view');
});

Route::get('/test', function () {
    return view('/test');
});


Route::post('/file', array('uses' => 'dashboard@store'));
Route::post('/fileStore', array('uses' => 'dashboard@storefile'));


Route::get('generate-pdf', 'dashboard@pdfview')->name('generate-pdf');
//Route::get('testAB', 'dashboard@testAB');

Route::get('generate-pdf/{id}', 'dashboard@pdfview')->name('generate-pdf');

Route::get('/home', 'dashboard@index')->name('home');
Route::get('create_request', 'dashboard@createRequest');

Route::get('/updateNotification', 'dashboard@updateNotification');

Route::get('notifications', 'dashboard@supplier_Notifications');
Route::get('response', 'dashboard@supplier_Response');
Route::get('supplier_all_request', 'dashboard@all_requests');

Route::get('post_request/{id}', 'dashboard@postRequest');
Route::get('view_request', 'dashboard@viewRequest');
Route::get('view/{id}', 'dashboard@view');

//Route::get('/bankDecline', array('uses' =>'dashboard@bankDecline'));

Route::get('supplier_accept/{id}', 'dashboard@supplierAccept');
Route::get('supplier_decline/{id}', 'dashboard@supplierDecline');
Route::get('banker_decline/{id}', 'dashboard@bankDecline');
//Route::post('/bankDecline', array('uses' => 'dashboard@bankDecline'));
Route::post('/createInvoice', array('uses' => 'dashboard@addInvoice'));
Route::post('/placeBid', array('uses' => 'dashboard@placeBid'));
Route::post('/bidType', array('uses' => 'dashboard@bidType'));
Route::post('/viewQuotes/{id}', array('uses' => 'dashboard@viewQuotes'));

/*Supplier Links*/
Route::get('create-invoice', 'dashboard@createInvoice');
Route::get('user-profile', 'dashboard@userProfile');
Route::get('dashboard', 'dashboard@dashboard');
Route::get('contact', 'dashboard@contact');
Route::get('response', 'dashboard@supplier_Response');
Route::get('notifications', 'dashboard@notifications');
Route::get('all-invoices', 'dashboard@supplierAllRequests');
Route::get('update-profile', 'dashboard@updateProfile');
Route::post('send-invites', array('uses' => 'dashboard@sendInvites'));
Route::get('sign-invites', function () {
    return view('/Supplier_Dashboard/sign_requests');
});


/* Routes for mobile app */
Route::post('mobile-app/login', array('uses' => 'mobilecontroller@loginAndSaveDeviceCreds'));
Route::get('/abcd', 'mobilecontroller@myTest');
//Route::post('mobile-app/login', 'mobilecontroller@loginAndSaveDeviceCreds');
Route::get('mobile-app/fetch-myinvoices', 'mobilecontroller@fetchmyInvoices');
Route::get('mobile-app/fetch-invoices', 'mobilecontroller@fetchotherInvoices');
Route::get('mobile-app/fetch-unsined-invoices', 'mobilecontroller@fetchUnsignedInvoices');
Route::get('mobile-app/fetch-all-signers', 'mobilecontroller@fetchSigners');


Route::get('mobile-app/fetch-notifications', 'mobilecontroller@fetchNotifications');
Route::get('mobile-app/fetch-user-data', 'mobilecontroller@fetchUsersInfo');
Route::post('mobile-app/create-invoice', array('uses' => 'mobilecontroller@createInvoice'));
Route::get('mobile-app/add-signers', 'mobilecontroller@addSigners');
Route::get('mobile-app/make-sign', 'mobilecontroller@makeSign');
Route::get('mobile-app/invoice-declined', 'mobilecontroller@invoiceDeclined');
Route::get('mobile-app/creator-sign', 'mobilecontroller@creatorSign');
Route::get('mobile-app/check-password', 'mobilecontroller@checkPassword');
Route::get('mobile-app/remove-notification', 'mobilecontroller@removeNotification');
Route::post('testAB', array('uses' => 'dashboard@testAB'));
Route::post('mobile-app/app-registration', array('uses' => 'mobilecontroller@appRegistration'));
Route::post('mobile-app/app-upload', array('uses' => 'mobilecontroller@uploadTest'));

/* Admin's Routes*/
Route::get('all-available-invoices', function () {
    return view('/admin/all_requests');
});
Route::get('all-users', function () {
    return view('/admin/all_users');
});



//buyer Signature via email
Route::get('buyer-signature/{id}', array('uses' => 'HomeController@buyerSignature'));

//Buyer Signature Reminder mail
Route::post('reminderMail', array('uses' => 'dashboard@reminderMailBuyer'));


