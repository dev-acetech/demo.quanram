@extends('theme.default')


@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Create Invoice</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

<!------ Include the above in your HEAD tag ---------->
 <div class="container">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Invoice</h4>
            <p class="card-category">Create your Invoice</p>
        </div>
        <div class="card-body">
             <form  method="post" action="/createInvoice" enctype="multipart/form-data" class="well span8" name="create_invoice">
                 <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="row">
                   <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Payee Name</label>
                                <input class="form-control" placeholder="" type="text" name="payee" required="required">
                            </div>
                    </div>
                    
                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Payer Name</label>
                                <input class="form-control" placeholder="" type="text" name="payer" required="required">
                            </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Ammount in numbers</label>
                                <input class="form-control" placeholder="" type="text" name="amount_numbers" required="required">
                            </div>
                    </div>
                     <div class="col-md-8">
                        <div class="form-group">
                                <label class="bmd-label-floating">Ammount in words</label>
                                <input class="form-control" placeholder="" type="text" name="amount_words" required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                     <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">No. of Days</label>
                                <input class="form-control" placeholder="" type="text" name="no_of_days" required="required">
                            </div>
                    </div>
                     <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Start Date</label>
                                <input class="form-control" placeholder="" type="date" name="start_payment_date" required="required">
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">End Date</label>
                                <input class="form-control" placeholder="" type="date" name="end_payment_date" required="required">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Buyer's Name</label>
                                <input class="form-control" placeholder="" type="text" name="buyer_name" required="required">
                            </div>
                    </div>
                    <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Buyer's Email</label>
                                <input class="form-control" placeholder="" type="email" name="buyers_email" required="required">
                            </div>
                    </div>
                     <div class="col-md-4">
                            <div class="form-group">
                                <label class="bmd-label-floating">Buyer's Address</label>
                                <input class="form-control" placeholder="" type="text" name="buyer_address" required="required">
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                <label class="bmd-label-floating">Supplier's Place</label>
                                <input class="form-control" placeholder="" type="text" name="supplier_place" required="required">
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Buyer's Place</label>
                                <input class="form-control" placeholder="" type="text" name="buyers_place" required="required">
                            </div>
                    </div>
              </div>
               <div class="row">
                     <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Bill No.</label>
                                <input class="form-control" placeholder="" type="text" name="bill_no" required="required">
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Bill Date</label>
                                <input class="form-control" placeholder="" type="date" name="bill_date" required="required">
                            </div>
                    </div>       
                </div>
                 <div class="row">
                    <div class="col-md-12">
                        <div class="input-group control-group increment" >
                              <div class="input-group-btn"> 
                                <button class="btn btn-success" type="button"><i class="fa fa-plus" style="    margin-right: 10px;"></i>Add Files</button>
                              </div>
                        </div>
                        <div class="clone hide">
                          <div class="control-group input-group" style="margin-top:10px">
                            <input type="file" name="filename[]" class="form-control">
                            <div class="input-group-btn"> 
                              <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>* Irrevocable Bill of Exchange</label>
                            <br>
                            <label>* Unconditional payment Order</label>
                            <br>
                            <label>* Attach only pdf | doc | png | jpeg</label>
                        </div>
                    </div>
                </div>
                
                    <button type="submit" class="btn btn-primary" style="margin-top:10px">Submit</button>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>
@endsection