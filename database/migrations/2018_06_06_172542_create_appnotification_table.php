<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppnotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appnotification', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reqId');
            $table->string('from');
            $table->string('to');
            $table->string('msg');
            $table->string('is_read');
            $table->string('notification_id');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appnotification');
    }
}
