@extends('theme.default')


@section('content')

<div class="row">

    <div class="col-lg-12">

        <h3 class="page-header">Quotes</h3>

    </div>

    <!-- /.col-lg-12 -->

</div>

<?php  
$id = \Request::segment(2);
$request_data = DB::table('request')
                ->where('id','=',$id)
                ->first();
$attchments = unserialize($request_data->attachments);
if(!empty($attchments['NA'])){
    $attchments = [];
}
if($bid_data = DB::table('placedbid')
                ->where(['req_id'=>$id ])
                //->where(['req_id'=>$id , 'user_id' => !Auth::user()->id])
                ->get()){
}

?>

  <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-tabs card-header-primary">
                                    <div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            <span class="nav-tabs-title">Tasks:</span>
                                            <ul class="nav nav-tabs" data-tabs="tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#profile" data-toggle="tab">
                                                        <i class="material-icons">bug_report</i> Invoice
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#messages" data-toggle="tab">
                                                        <i class="material-icons">code</i> Supplier
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#settings" data-toggle="tab">
                                                        <i class="material-icons">cloud</i> Buyer
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                @if(!empty($attchments))
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#attachments" data-toggle="tab">
                                                        <i class="material-icons">code</i> Attachments
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="profile">
                                           <div id="request_view" style="width: 100%" class="container request_view well ">
                                                <div class="row">
                                                    <div class="col-md-12"><h3 class="center highlight"><b>Bill of Exchange</b></h3></div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="normal_color">{{$request_data->supplier_place}},</label>
                                                            <label class="normal_color"> {{date('d-m-Y', strtotime($request_data->created_at))}}</label>
                                                        </div>
                                                    </div>
                                                 <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="highlight">Amount</label>
                                                            <label class="normal_color">IN Rs.{{$request_data->amount_numbers}}/-</label>
                                                        </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                            <label class="highlight">At</label>
                                                            <label class="normal_color">{{$request_data->no_of_days}} days after sight</label>
                                                        </div>
                                                </div>
                                                <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label class="highlight">Pay against this Sole Bill of Exchange to the order of</label>
                                                        </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="normal_color">Ourselves</label>
                                                        </div>
                                                </div>
                                                 <div class="col-md-6">
                                                </div>
                                                <div class="col-md-12"></div>
                                                <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="highlight">the sum of</label>
                                                        </div>
                                                </div>
                                                 <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label class="normal_color">In Rs. {{$request_data->amount_words}}</label>
                                                        </div>
                                                </div>
                                                <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="highlight">for value</label>
                                                        </div>
                                                </div>
                                                 <div class="col-md-9">
                                                        <div class="form-group">
                                                            <label class="normal_color">Received</label>
                                                        </div>
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="highlight">To:</label>
                                                            <p>{{$request_data->buyer_address}}</p>
                                                            <p>{{$request_data->buyer_name}}</p>
                                                             <p>{{$request_data->buyers_email}}</p>
                                                        </div>
                                                </div>
                                                 <div class="col-md-6 center">
                                                        <div class="form-group">
                                                            <label class="highlight">For and on behalf of:</label>
                                                            <p>{{$request_data->payee}}</p>
                                                            <p>{{$request_data->supplier_place}}</p>
                                                        </div>
                                                </div>

                                                </div>

                                                <div class="row">
                                                
                                                
                                               
                                                </div>
                                        </div>
                                        </div>
                                        <div class="tab-pane" id="messages">
                                            <table class="table table-hover">
                                                <thead class="text-warning">
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Amount</th>
                                                    <th>Email</th>
                                                    <th>City</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$request_data->payee}}</td>
                                                        <td>{{$request_data->amount_numbers}}</td>
                                                        <td>{{$request_data->supplier_email}}</td>
                                                        <td>{{$request_data->supplier_place}}</td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                             <table class="table table-hover">
                                                <thead class="text-warning">
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>City</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td>{{$request_data->buyer_name}}</td>
                                                        <td>{{$request_data->buyers_email}}</td>
                                                        <td>{{$request_data->buyers_place}}</td>
                                                    </tr>                                            
                                                </tbody>
                                            </table>
                                        </div>
                                        @if(!empty($attchments))
                                        <div class="tab-pane" id="attachments">
                                                <div class="row">
                                                    @foreach($attchments as $atch)
                                                    <div class="col-md-4">
                                                        @php $ext = pathinfo($atch, PATHINFO_EXTENSION); @endphp
                                                        @if($ext == 'pdf')
                                                        <object data='{{$atch}}' 
                                                            type='application/pdf' 
                                                            width='100%' 
                                                            height='100%'>
                                                            <p style="    height: 100%;margin-bottom: 0">PDF view is not supported in this browsrer, please open in another one.</p>
                                                        </object>
                                                        @elseif($ext == 'png')
                                                        <img  width="100%" height="100%" src="{{$atch}}">
                                                        @else
                                                        <img  width="100%"  height="100%" src="{{$atch}}">
                                                        @endif
                                                        <button  style="position: absolute;    bottom: 0;left: 0;" data-url="{{$atch}}" class="btn btn-info view" >View</button>
                                                    </div>
                                                    @endforeach  
                                                </div>
                                        </div>
                                         @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($request_data->invoice_type != 'closeBids')
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header card-header-warning">
                                    <h4 class="card-title">Live Quotes</h4>
                                    <p class="card-category"></p>
                                </div>
                                <div class="card-body table-responsive">
                                     <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Rate</th>
                                            <th>Status</th>
                                        </thead>
                                        <tbody>
                                            <?php $i = 0; if(count($bid_data) > 0 ){
                                                foreach ($bid_data as $k => $v) {  $i++; ?>
                                                   
                                                     <tr>
                                                        <td>{{$i}}</td>
                                                        <td>Bidder {{$i}}</td>
                                                        <td>{{$v->bidBudget}}</td>
                                                        <td>{{$v->bidRate}}</td>
                                                        <td>{{$v->status}}</td>
                                                    </tr>  
                                                    
                                            <?php  }
                                            } else { ?>
                                                <tr>
                                                    <td colspan="5">No Quotes placed.</td>
                                                </tr>
                                           <?php } ?>
                                                                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
<div id="myModal" class="modal" style="padding-top:50px;">
      <span style="opacity: 1;" class="close">&times;</span>
       <img class="modal-content" id="img01">
        <object id="img02"  data="" type="application/pdf" width="100%" height="850">
            <p style="color: white;
        text-align: center;" class="err">
                It appears your Web browser is not configured to display PDF files. No worries, just <a class="downloadPdf" href="">click here to download the PDF file.</a>
            </p>
        </object>
    </div>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 12vh;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
<script>
var modal;
var modalImg;
var modalSrc;
$(document).ready(function(){
modal = document.getElementById('myModal');
modalImg = document.getElementById("img01");
modalSrc = document.getElementById("img02");
    $('.view').click(function(){
        var fileURL =$(this).attr('data-url');
        var ext = fileURL.substr(fileURL.lastIndexOf('.') + 1);
        $('#myModal').show();
        $('.card .card-header').attr('style', 'z-index:0 !important');
        if(ext == 'pdf'){
            $('.err').show();
            document.getElementById("img01").src = '';
            document.getElementById("img02").data = fileURL+'#view=Fit';
            
            $('.downloadPdf').attr('href',fileURL);
            modalImg = '';
        }else{
             $('.err').hide();
            document.getElementById("img01").src = fileURL;
            document.getElementById("img02").data = '';
            modalSrc = '';
        }
        console.log(modalSrc,modalImg)    
        
        
    });
   

    $('.close').click(function(){
        console.log(1245)
        $('#myModal').hide();
        $('.card .card-header').attr('style', 'z-index:3 !important');
        modalImg = '';
        modalSrc = '';
    });
});
</script>
@endsection