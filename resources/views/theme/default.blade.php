<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/img/apple-icon.png">
    <link rel="icon" href="/img/favicon.png">
    <title>
        Invoice | Klathrate
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/css/material-dashboard.css?v=2.0.0">
     <link href="/css/font-awesome.css" rel="stylesheet" />
    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Documentation extras -->
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="/css/demo.css" rel="stylesheet" />
     <link href="/css/style.css" rel="stylesheet" />
   <script src="/js/jquery.js"></script>
   

</head>

<body class="">
    <div class="wrapper">
          @include('theme.sidebar')

            <div class="main-panel">
                 @include('theme.header')

          


        <div id="page-wrapper" class="container-fluid">

            @yield('content')

        </div>

        <!-- /#page-wrapper -->

         <footer class="footer ">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="https://www.creative-tim.com">
                                        Klathrate
                                    </a>
                                </li>
                                <li>
                                    <a href="http://presentation.creative-tim.com">
                                        About Us
                                    </a>
                                </li>
                                <li>
                                    <a href="http://blog.creative-tim.com">
                                        Blog
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.creative-tim.com/license">
                                        Licenses
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <div class="copyright pull-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>, made with love by
                            <a href="http://www.acetechventures.in/" target="_blank">ACETECHVENTURES</a> for a better web.
                        </div>
                    </div>
                </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="/js/core/jquery.min.js"></script>
<script src="/js/core/popper.min.js"></script>
<script src="/js/bootstrap-material-design.js"></script>
<script src="/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
<script src="/js/plugins/chartist.min.js"></script>
<!-- Library for adding dinamically elements -->
<script src="/js/plugins/arrive.min.js" type="text/javascript"></script>
<!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
<script src="/js/plugins/bootstrap-notify.js"></script>
<!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
<script src="/js/material-dashboard.js?v=2.0.0"></script>
<!-- demo init -->
<script src="/js/plugins/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        //init wizard

        // demo.initMaterialWizard();

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

        demo.initCharts();

    });
</script>
<script type="text/javascript">
            //alert($('.sidebar-wrapper').val());
            $('.sidebar-wrapper .nav-link').click(function(){
                //$('.sidebar-wrapper a').removeClass('active');
                console.log($(this).prev());
            });
        </script>
