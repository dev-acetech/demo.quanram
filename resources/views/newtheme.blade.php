@extends('theme.klathrate')

@section('content')

                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                            <div class="toggler"> </div>
                            <div class="toggler-close"> </div>
                            <div class="theme-options">
                                <div class="theme-option theme-colors clearfix">
                                    <span> THEME COLOR </span>
                                    <ul>
                                        <li class="color-default current tooltips" data-style="default" data-container="body" data-original-title="Default"> </li>
                                        <li class="color-darkblue tooltips" data-style="darkblue" data-container="body" data-original-title="Dark Blue"> </li>
                                        <li class="color-blue tooltips" data-style="blue" data-container="body" data-original-title="Blue"> </li>
                                        <li class="color-grey tooltips" data-style="grey" data-container="body" data-original-title="Grey"> </li>
                                        <li class="color-light tooltips" data-style="light" data-container="body" data-original-title="Light"> </li>
                                        <li class="color-light2 tooltips" data-style="light2" data-container="body" data-html="true" data-original-title="Light 2"> </li>
                                    </ul>
                                </div>
                                <div class="theme-option">
                                    <span> Theme Style </span>
                                    <select class="layout-style-option form-control input-sm">
                                        <option value="square" selected="selected">Square corners</option>
                                        <option value="rounded">Rounded corners</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Layout </span>
                                    <select class="layout-option form-control input-sm">
                                        <option value="fluid" selected="selected">Fluid</option>
                                        <option value="boxed">Boxed</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Header </span>
                                    <select class="page-header-option form-control input-sm">
                                        <option value="fixed" selected="selected">Fixed</option>
                                        <option value="default">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Top Menu Dropdown</span>
                                    <select class="page-header-top-dropdown-style-option form-control input-sm">
                                        <option value="light" selected="selected">Light</option>
                                        <option value="dark">Dark</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Mode</span>
                                    <select class="sidebar-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Menu </span>
                                    <select class="sidebar-menu-option form-control input-sm">
                                        <option value="accordion" selected="selected">Accordion</option>
                                        <option value="hover">Hover</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Style </span>
                                    <select class="sidebar-style-option form-control input-sm">
                                        <option value="default" selected="selected">Default</option>
                                        <option value="light">Light</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Sidebar Position </span>
                                    <select class="sidebar-pos-option form-control input-sm">
                                        <option value="left" selected="selected">Left</option>
                                        <option value="right">Right</option>
                                    </select>
                                </div>
                                <div class="theme-option">
                                    <span> Footer </span>
                                    <select class="page-footer-option form-control input-sm">
                                        <option value="fixed">Fixed</option>
                                        <option value="default" selected="selected">Default</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Admin Dashboard
                            <!-- <small>statistics, charts, recent events and reports</small> -->
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
                                    <div class="visual">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="1349">0</span>
                                        </div>
                                        <div class="desc"> New Feedbacks </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="12,5">0</span>M$ </div>
                                        <div class="desc"> Total Profit </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                                    <div class="visual">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span data-counter="counterup" data-value="549">0</span>
                                        </div>
                                        <div class="desc"> New Orders </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                                    <div class="visual">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number"> +
                                            <span data-counter="counterup" data-value="89"></span>% </div>
                                        <div class="desc"> Brand Popularity </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-cursor font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">General Stats</span>
                                        </div>
                                        <div class="actions">
                                            <a href="javascript:;" class="btn btn-sm btn-circle red easy-pie-chart-reload">
                                                <i class="fa fa-repeat"></i> Reload </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="easy-pie-chart">
                                                    <div class="number transactions" data-percent="55">
                                                        <span>+55</span>% </div>
                                                    <a class="title" href="javascript:;"> Transactions
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10 visible-sm"> </div>
                                            <div class="col-md-4">
                                                <div class="easy-pie-chart">
                                                    <div class="number visits" data-percent="85">
                                                        <span>+85</span>% </div>
                                                    <a class="title" href="javascript:;"> New Visits
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10 visible-sm"> </div>
                                            <div class="col-md-4">
                                                <div class="easy-pie-chart">
                                                    <div class="number bounce" data-percent="46">
                                                        <span>-46</span>% </div>
                                                    <a class="title" href="javascript:;"> Bounce
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-equalizer font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Server Stats</span>
                                            <span class="caption-helper">monthly stats...</span>
                                        </div>
                                        <div class="tools">
                                            <a href="" class="collapse"> </a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                            <a href="" class="reload"> </a>
                                            <a href="" class="remove"> </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="sparkline-chart">
                                                    <div class="number" id="sparkline_bar5"></div>
                                                    <a class="title" href="javascript:;"> Network
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10 visible-sm"> </div>
                                            <div class="col-md-4">
                                                <div class="sparkline-chart">
                                                    <div class="number" id="sparkline_bar6"></div>
                                                    <a class="title" href="javascript:;"> CPU Load
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="margin-bottom-10 visible-sm"> </div>
                                            <div class="col-md-4">
                                                <div class="sparkline-chart">
                                                    <div class="number" id="sparkline_line"></div>
                                                    <a class="title" href="javascript:;"> Load Rate
                                                        <i class="icon-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">
                                            <i class="icon-bubbles font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Recent Notifications</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#portlet_comments_1" data-toggle="tab"> Pending </a>
                                            </li>
                                            <li>
                                                <a href="#portlet_comments_2" data-toggle="tab"> Approved </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="portlet_comments_1">
                                                <!-- BEGIN: Comments -->
                                                <div class="mt-comments">
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">Pending</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar6.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Larisa Maskalyova</span>
                                                                <span class="mt-comment-date">12 Feb, 08:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> It is a long established fact that a reader will be distracted. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-rejected">Rejected</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar8.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Natasha Kim</span>
                                                                <span class="mt-comment-date">19 Dec,09:50 AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> The generated Lorem or non-characteristic Ipsum is therefore or non-characteristic. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-pending">Pending</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar4.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Sebastian Davidson</span>
                                                                <span class="mt-comment-date">10 Dec, 09:20 AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> The standard chunk of Lorem or non-characteristic Ipsum used since the 1500s or non-characteristic. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-rejected">Rejected</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END: Comments -->
                                            </div>
                                            <div class="tab-pane" id="portlet_comments_2">
                                                <!-- BEGIN: Comments -->
                                                <div class="mt-comments">
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar4.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Michael Baker</span>
                                                                <span class="mt-comment-date">26 Feb, 10:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-approved">Approved</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar8.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Larisa Maskalyova</span>
                                                                <span class="mt-comment-date">12 Feb, 08:30AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> It is a long established fact that a reader will be distracted by. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-approved">Approved</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar6.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Natasha Kim</span>
                                                                <span class="mt-comment-date">19 Dec,09:50 AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc. </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-approved">Approved</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-comment">
                                                        <div class="mt-comment-img">
                                                            <img src="/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-comment-body">
                                                            <div class="mt-comment-info">
                                                                <span class="mt-comment-author">Sebastian Davidson</span>
                                                                <span class="mt-comment-date">10 Dec, 09:20 AM</span>
                                                            </div>
                                                            <div class="mt-comment-text"> The standard chunk of Lorem Ipsum used since the 1500s </div>
                                                            <div class="mt-comment-details">
                                                                <span class="mt-comment-status mt-comment-status-approved">Approved</span>
                                                                <ul class="mt-comment-actions">
                                                                    <li>
                                                                        <a href="#">Quick Edit</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">View</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">Delete</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END: Comments -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">
                                            <i class=" icon-social-twitter font-dark hide"></i>
                                            <span class="caption-subject font-dark bold uppercase">Quick Actions</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_actions_pending" data-toggle="tab"> Pending </a>
                                            </li>
                                            <li>
                                                <a href="#tab_actions_completed" data-toggle="tab"> Completed </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_actions_pending">
                                                <!-- BEGIN: Actions -->
                                                <div class="mt-actions">
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar10.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-magnet"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Natasha Kim</span>
                                                                        <p class="mt-action-desc">Dummy text of the printing</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar3.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class=" icon-bubbles"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Gavin Bond</span>
                                                                        <p class="mt-action-desc">pending for approval</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-red"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar2.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-call-in"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Diana Berri</span>
                                                                        <p class="mt-action-desc">Lorem Ipsum is simply dummy text</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar7.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class=" icon-bell"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">John Clark</span>
                                                                        <p class="mt-action-desc">Text of the printing and typesetting industry</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-red"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar8.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-magnet"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Donna Clarkson </span>
                                                                        <p class="mt-action-desc">Simply dummy text of the printing</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar9.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-magnet"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Tom Larson</span>
                                                                        <p class="mt-action-desc">Lorem Ipsum is simply dummy text</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END: Actions -->
                                            </div>
                                            <div class="tab-pane" id="tab_actions_completed">
                                                <!-- BEGIN:Completed-->
                                                <div class="mt-actions">
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar1.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-action-redo"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Frank Cameron</span>
                                                                        <p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-red"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar8.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-cup"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Ella Davidson </span>
                                                                        <p class="mt-action-desc">Text of the printing and typesetting industry</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar5.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class=" icon-graduation"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Jason Dickens </span>
                                                                        <p class="mt-action-desc">Dummy text of the printing and typesetting industry</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-red"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                            <img src="/pages/media/users/avatar2.jpg" /> </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-icon ">
                                                                        <i class="icon-badge"></i>
                                                                    </div>
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">Jan Kim</span>
                                                                        <p class="mt-action-desc">Lorem Ipsum is simply dummy</p>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-action-datetime ">
                                                                    <span class="mt-action-date">3 jun</span>
                                                                    <span class="mt-action-dot bg-green"></span>
                                                                    <span class="mt=action-time">9:30-13:00</span>
                                                                </div>
                                                                <div class="mt-action-buttons ">
                                                                    <div class="btn-group btn-group-circle">
                                                                        <button type="button" class="btn btn-outline green btn-sm">Appove</button>
                                                                        <button type="button" class="btn btn-outline red btn-sm">Reject</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- END: Completed -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                       <!--  <div class="row">
                             <div class="col-lg-6 col-xs-12 col-sm-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-bubble font-dark hide"></i>
                                            <span class="caption-subject font-hide bold uppercase">Recent Users</span>
                                        </div>
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="javascript:;"> Option 1</a>
                                                    </li>
                                                    <li class="divider"> </li>
                                                    <li>
                                                        <a href="javascript:;">Option 2</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 3</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">Option 4</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="mt-widget-1">
                                                    <div class="mt-icon">
                                                        <a href="#">
                                                            <i class="icon-plus"></i>
                                                        </a>
                                                    </div>
                                                    <div class="mt-img">
                                                        <img src="/pages/media/users/avatar80_8.jpg"> </div>
                                                    <div class="mt-body">
                                                        <h3 class="mt-username">Diana Ellison</h3>
                                                        <p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
                                                        <div class="mt-stats">
                                                            <div class="btn-group btn-group btn-group-justified">
                                                                <a href="javascript:;" class="btn font-red">
                                                                    <i class="icon-bubbles"></i> 1,7k </a>
                                                                <a href="javascript:;" class="btn font-green">
                                                                    <i class="icon-social-twitter"></i> 2,6k </a>
                                                                <a href="javascript:;" class="btn font-yellow">
                                                                    <i class="icon-emoticon-smile"></i> 3,7k </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mt-widget-1">
                                                    <div class="mt-icon">
                                                        <a href="#">
                                                            <i class="icon-plus"></i>
                                                        </a>
                                                    </div>
                                                    <div class="mt-img">
                                                        <img src="/pages/media/users/avatar80_7.jpg"> </div>
                                                    <div class="mt-body">
                                                        <h3 class="mt-username">Jason Baker</h3>
                                                        <p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
                                                        <div class="mt-stats">
                                                            <div class="btn-group btn-group btn-group-justified">
                                                                <a href="javascript:;" class="btn font-yellow">
                                                                    <i class="icon-bubbles"></i> 1,7k </a>
                                                                <a href="javascript:;" class="btn font-blue">
                                                                    <i class="icon-social-twitter"></i> 2,6k </a>
                                                                <a href="javascript:;" class="btn font-green">
                                                                    <i class="icon-emoticon-smile"></i> 3,7k </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mt-widget-1">
                                                    <div class="mt-icon">
                                                        <a href="#">
                                                            <i class="icon-plus"></i>
                                                        </a>
                                                    </div>
                                                    <div class="mt-img">
                                                        <img src="/pages/media/users/avatar80_6.jpg"> </div>
                                                    <div class="mt-body">
                                                        <h3 class="mt-username">Julia Berry</h3>
                                                        <p class="mt-user-title"> Lorem Ipsum is simply dummy text. </p>
                                                        <div class="mt-stats">
                                                            <div class="btn-group btn-group btn-group-justified">
                                                                <a href="javascript:;" class="btn font-yellow">
                                                                    <i class="icon-bubbles"></i> 1,7k </a>
                                                                <a href="javascript:;" class="btn font-red">
                                                                    <i class="icon-social-twitter"></i> 2,6k </a>
                                                                <a href="javascript:;" class="btn font-green">
                                                                    <i class="icon-emoticon-smile"></i> 3,7k </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div> -->
                        
                        

@endsection


