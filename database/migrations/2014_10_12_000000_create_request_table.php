<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_id');
            $table->string('user_id');
           
            $table->string('payee');
            $table->string('payer');
            $table->string('amount_numbers');

            $table->string('amount_words');
            $table->string('no_of_days');
            $table->string('start_payment_date');

            $table->string('end_payment_date');
            $table->string('supplier_email');
            $table->string('buyer_name');

            $table->string('buyers_email');
            $table->string('buyer_address');
            $table->string('supplier_place');

            $table->string('buyers_place');
            $table->string('bill_no');
            $table->string('bill_date');
            
            $table->string('attachments');

            $table->string('status');
            $table->string('is_posted');  
            $table->string('invoice_type'); 
            $table->string('invoice_number');
            $table->string('is_signed');
            $table->string('otp');  
             $table->string('is_invite_sent');  
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request');
    }
}
