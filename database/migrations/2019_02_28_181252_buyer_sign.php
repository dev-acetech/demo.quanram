<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuyerSign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyersign', function (Blueprint $table) {
            $table->increments('id');
            $table->string('request_id');
            $table->string('user_id');
            $table->string('signature_number');
            $table->string('buyer_email');
            $table->string('buyer_sign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyersign');
    }
}
